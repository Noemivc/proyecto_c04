
package modelo;

public class claseCurso {
    
     private int id_curso;
    private String nombre;
    private int nro_hora;
    private int nro_creditos;

    public claseCurso(int id_curso, String nombre, int nro_hora, int nro_creditos) {
        this.id_curso = id_curso;
        this.nombre = nombre;
        this.nro_hora = nro_hora;
        this.nro_creditos = nro_creditos;
    }

    public claseCurso(String nombre, int nro_hora, int nro_creditos) {
        this.nombre = nombre;
        this.nro_hora = nro_hora;
        this.nro_creditos = nro_creditos;
    }

    public int getId_curso() {
        return id_curso;
    }

    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNro_hora() {
        return nro_hora;
    }

    public void setNro_hora(int nro_hora) {
        this.nro_hora = nro_hora;
    }

    public int getNro_creditos() {
        return nro_creditos;
    }

    public void setNro_creditos(int nro_creditos) {
        this.nro_creditos = nro_creditos;
    }
    
    
}
