
package modelo;

    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.util.ArrayList;
    import java.util.List;

public class modeloHorario 
{

    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    
    public List<claseHorario> obtenerHorario() throws Exception
    {
        List<claseHorario> horario = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM horario");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_horario = rs.getInt("id_horario");
                int id_estudiante = rs.getInt("id_estudiante");
                int id_carrera = rs.getInt("id_carrera");
                int id_cursos = rs.getInt("id_cursos");
                int id_docente = rs.getInt("id_docente");
                int id_aula = rs.getInt("id_aula");
                String dia = rs.getString("dia");
                String horainicio = rs.getString("horainicio");
                String horafin = rs.getString("horafin");
                claseHorario horarioTemporal = new claseHorario(id_horario,id_estudiante, id_carrera, id_cursos, id_docente, id_aula, dia, horainicio, horafin);
                horario.add(horarioTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return horario;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarHorario(claseHorario nuevoHorario) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO horario (id_estudiante, id_carrera, id_cursos, id_docente, id_aula, dia, horainicio, horafin) VALUES(?,?,?,?,?,?,?,?)");
            ps.setInt(1, nuevoHorario.getId_estudiante());
            ps.setInt(2, nuevoHorario.getId_carrera());           
            ps.setInt(3, nuevoHorario.getId_cursos());
            ps.setInt(4, nuevoHorario.getId_docente());
            ps.setInt(5, nuevoHorario.getId_aula());
            ps.setString(6, nuevoHorario.getDia());
            ps.setString(7, nuevoHorario.getHorainicio());
            ps.setString(8, nuevoHorario.getHorafin());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseHorario cargarHorario (int id) throws Exception
    {
        claseHorario horarioCargado = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM horario WHERE id_horario=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next())
            {
                int id_estudiante = rs.getInt("id_estudiante");
                int id_carrera = rs.getInt("id_carrera");
                int id_cursos = rs.getInt("id_cursos");
                int id_docente = rs.getInt("id_docente");
                int id_aula = rs.getInt("id_aula");
                String dia = rs.getString("dia");
                String horainicio = rs.getString("horainicio");
                String horafin = rs.getString("horafin");
                horarioCargado = new claseHorario(id, id_estudiante, id_carrera, id_cursos, id_docente, id_aula, dia, horainicio, horafin);
            }
            con.close();
            ps.close();
            rs.close();
            return horarioCargado;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarHorario(claseHorario horarioActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE horario SET id_estudiante=?, id_carrera=?,id_cursos=?,id_docente=?,id_aula=?,dia=?,horainicio=?,horafin=? WHERE id_horario=? ");
            ps.setInt(1, horarioActualizado.getId_estudiante());
            ps.setInt(2, horarioActualizado.getId_carrera());
            ps.setInt(3, horarioActualizado.getId_cursos());
            ps.setInt(4, horarioActualizado.getId_docente());
            ps.setInt(5, horarioActualizado.getId_aula());
            ps.setString(6, horarioActualizado.getDia());
            ps.setString(7, horarioActualizado.getHorainicio());
            ps.setString(8, horarioActualizado.getHorafin());
            ps.setInt(9, horarioActualizado.getId_horario());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
    }
    
    public void eliminarHorario(int id) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM horario WHERE id_horario=?");
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
}
