package modelo;
public class claseprovincia {
  private int id_provin;
  private int id_departamento;
  private String nombre;

    public claseprovincia(int id_provin, int id_departamento, String nombre) {
        this.id_provin = id_provin;
        this.id_departamento = id_departamento;
        this.nombre = nombre;
    }

    public claseprovincia(int id_departamento, String nombre) {
        this.id_departamento = id_departamento;
        this.nombre = nombre;
    }


    public int getId_provin() {
        return id_provin;
    }

    public void setId_provin(int id_provin) {
        this.id_provin = id_provin;
    }

    public int getId_departamento() {
        return id_departamento;
    }

    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
  
}
