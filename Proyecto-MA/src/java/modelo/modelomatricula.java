package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class modelomatricula {
   private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<clasematricula> obtenerMatricula() throws Exception
    {
        List<clasematricula> matricula = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM matricula");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_matricula = rs.getInt("id_matricula");
                int id_estudiantes = rs.getInt("id_estudiantes");
                String fecha = rs.getString("fecha");
                String ciclo = rs.getString("ciclo");
                String condiciones = rs.getString("condiciones");
                int id_Empleado = rs.getInt("id_Empleado");
                clasematricula matriculaTemporal = new clasematricula(id_matricula,id_estudiantes,fecha,ciclo,condiciones,id_Empleado);
                matricula.add(matriculaTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return matricula;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarMatricula(clasematricula nuevamatricula) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO matricula(id_estudiantes,fecha,ciclo,condiciones,id_Empleado) VALUES(?,?,?,?,?)");
            ps.setInt(1,nuevamatricula.getId_estudiantes());
            ps.setString(2, nuevamatricula.getFecha());
            ps.setString(3, nuevamatricula.getCiclo());
            ps.setString(4, nuevamatricula.getCondiciones());
            ps.setInt(5, nuevamatricula.getId_Empleado());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public clasematricula cargarMatricula(int id) throws Exception
    {
        clasematricula matriculaCargada = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");

            ps = con.prepareCall("SELECT * FROM matricula WHERE id_matricula=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next())
            {
                int id_estudiantes = rs.getInt("id_estudiantes");
                String fecha = rs.getString("fecha");
                String ciclo = rs.getString("ciclo");
                String condiciones = rs.getString("condiciones");
                int id_Empleado = rs.getInt("id_Empleado");
                matriculaCargada = new clasematricula(id,id_estudiantes,fecha,ciclo,condiciones,id_Empleado);
            }
            con.close();
            ps.close();
            rs.close();
            return matriculaCargada;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarMatricula(clasematricula matriculaActualizada) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE matricula SET id_estudiantes=?, fecha=?, ciclo=?, condiciones=?, id_empleado=?  WHERE id_matricula=?");
            ps.setInt(1, matriculaActualizada.getId_estudiantes());
            ps.setString(2, matriculaActualizada.getFecha());
            ps.setString(3, matriculaActualizada.getCiclo());
            ps.setString(4, matriculaActualizada.getCondiciones());
            ps.setInt(5, matriculaActualizada.getId_Empleado());
            ps.setInt(6, matriculaActualizada.getId_matricula());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void eliminarMatricula(int id) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("DELETE FROM matricula WHERE id_matricula=?");
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
      
}
