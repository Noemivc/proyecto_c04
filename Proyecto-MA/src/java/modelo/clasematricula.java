package modelo;
public class clasematricula {
   private int id_matricula;
   private int id_estudiantes;
   private String fecha;
   private String ciclo;
   private String condiciones;
   private int id_Empleado;

    public clasematricula(int id_matricula, int id_estudiantes, String fecha, String ciclo, String condiciones, int id_Empleado) {
        this.id_matricula = id_matricula;
        this.id_estudiantes = id_estudiantes;
        this.fecha = fecha;
        this.ciclo = ciclo;
        this.condiciones = condiciones;
        this.id_Empleado = id_Empleado;
    }

    public clasematricula(int id_estudiantes, String fecha, String ciclo, String condiciones, int id_Empleado) {
        this.id_estudiantes = id_estudiantes;
        this.fecha = fecha;
        this.ciclo = ciclo;
        this.condiciones = condiciones;
        this.id_Empleado = id_Empleado;
    }

    public int getId_matricula() {
        return id_matricula;
    }

    public void setId_matricula(int id_matricula) {
        this.id_matricula = id_matricula;
    }

    public int getId_estudiantes() {
        return id_estudiantes;
    }

    public void setId_estudiantes(int id_estudiantes) {
        this.id_estudiantes = id_estudiantes;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getCondiciones() {
        return condiciones;
    }

    public void setCondiciones(String condiciones) {
        this.condiciones = condiciones;
    }

    public int getId_Empleado() {
        return id_Empleado;
    }

    public void setId_Empleado(int id_Empleado) {
        this.id_Empleado = id_Empleado;
    }
   
    
}
