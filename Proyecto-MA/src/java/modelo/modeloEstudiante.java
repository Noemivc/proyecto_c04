
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class modeloEstudiante 
{
    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<claseEstudiante > obtenerEstudiante () throws Exception
    {
        List<claseEstudiante> estudiante = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM estudiante");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_estudiante = rs.getInt("id_estudiante");
                String dni = rs.getString("dni");
                String nombre = rs.getString("nombre");
                String apellidos = rs.getString("apellidos");
                String genero = rs.getString("genero");;
                String celular = rs.getString("celular");
                String correo = rs.getString("correo");
                String fecha_nac = rs.getString("fecha_nac");
                String lugar_nac = rs.getString("lugar_nac");
                int id_distrito = rs.getInt("id_distrito");
                String direccion = rs.getString("direccion");
                String estado = rs.getString("estado");
                claseEstudiante EstudianteTemporal = new claseEstudiante(id_estudiante, dni, nombre, apellidos, genero, celular,correo,fecha_nac,lugar_nac
                ,id_distrito,direccion,estado);
                estudiante.add(EstudianteTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return estudiante;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarEstudiante(claseEstudiante nuevoEstudiante) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO estudiante (dni,nombre,apellidos,genero,celular,correo,fecha_nac,lugar_nac,id_distrito,direccion,estado)"
                                               + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, nuevoEstudiante.getDni());
            ps.setString(2, nuevoEstudiante.getNombre());
            ps.setString(3, nuevoEstudiante.getApellidos());
            ps.setString(4, nuevoEstudiante.getGenero());
            ps.setString(5, nuevoEstudiante.getCelular());
            ps.setString(6, nuevoEstudiante.getCorreo());
            ps.setString(7, nuevoEstudiante.getFecha_nac());
            ps.setString(8, nuevoEstudiante.getLugar_nac());
            ps.setInt(9, nuevoEstudiante.getId_distrito());
            ps.setString(10, nuevoEstudiante.getDireccion());
            ps.setString(11, nuevoEstudiante.getEstado());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseEstudiante cargarEstudiante (int id) throws Exception
    {
        claseEstudiante estudianteCargado = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM estudiante WHERE id_estudiante=? ");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String dni = rs.getString("dni");
                String nombre = rs.getString("nombre");
                String apellidos = rs.getString("apellidos");
                String genero = rs.getString("genero");
                String celular = rs.getString("celular");
                String correo = rs.getString("correo");
                String fecha_nac = rs.getString("fecha_nac");
                String lugar_nac = rs.getString("lugar_nac");
                int id_distrito = rs.getInt("id_distrito");
                String direccion = rs.getString("direccion");
                String estado = rs.getString("estado");
                estudianteCargado = new claseEstudiante(id, dni, nombre, apellidos, genero, celular,
                correo,fecha_nac,lugar_nac,id_distrito,direccion,estado);
            }
            con.close();
            ps.close();
            rs.close();
            return estudianteCargado;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarEstudiante(claseEstudiante EstudianteActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE estudiante SET dni=?,nombre=?,apellidos=?,genero=?,celular=?,correo=?,fecha_nac=?,lugar_nac=?,id_distrito=?,direccion=?,estado=? WHERE id_estudiante=? ");
            ps.setString(1, EstudianteActualizado.getDni());
            ps.setString(2, EstudianteActualizado.getNombre());
            ps.setString(3, EstudianteActualizado.getApellidos());
            ps.setString(4, EstudianteActualizado.getGenero());
            ps.setString(5, EstudianteActualizado.getCelular());
            ps.setString(6, EstudianteActualizado.getCorreo());
            ps.setString(7, EstudianteActualizado.getFecha_nac());
            ps.setString(8, EstudianteActualizado.getLugar_nac());
            ps.setInt(9, EstudianteActualizado.getId_distrito());
            ps.setString(10, EstudianteActualizado.getDireccion());
            ps.setString(11, EstudianteActualizado.getEstado());
            ps.setInt(12, EstudianteActualizado.getId_estudiante());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void eliminarEstudiante(int id) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM estudiante WHERE id_estudiante=?");
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombre = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from estudiante where id_estudiante=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombre = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombre;
    }
    
}

