
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class modeloDistrito 
{
    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;

    public List<claseDistrito> obtenerDistrito() throws Exception {
        List<claseDistrito> distritos = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat", "root", "");
            ps = con.prepareCall("SELECT * FROM distrito");
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                int id_distri = rs.getInt("id_distri");
                int id_provincia = rs.getInt("id_provincia");
                String nombre = rs.getString("nombre");
                claseDistrito distritoTemporal = new claseDistrito(id_distri, id_provincia, nombre);
                distritos.add(distritoTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return distritos;
        } catch (SQLException e) {
            throw e;
        }
    }

    public void registrarDistrito(claseDistrito nuevodistrito) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat", "root", "");
            ps = con.prepareCall("INSERT INTO distrito(id_provincia,nombre) VALUES (?,?)");
            ps.setInt(1, nuevodistrito.getId_provincia());
            ps.setString(2, nuevodistrito.getNombre());
            ps.executeUpdate();
            con.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        }
    }

    public claseDistrito cargarDistrito(int id) throws Exception {
        claseDistrito distritocargado = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat", "root", "");
            ps = con.prepareCall(" SELECT * FROM distrito WHERE id_distri=? ");
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                int id_provincia = rs.getInt("id_provincia");
                String nombre = rs.getString("nombre");

                distritocargado = new claseDistrito(id_provincia, nombre);
            }
            con.close();
            ps.close();
            rs.close();
            return distritocargado;
        } catch (SQLException e) {
            throw e;
        }
    }

    public void actualizarDistrito(claseDistrito distritoActualizado) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat", "root", "");
            ps = con.prepareCall("UPDATE distrito SET id_provincia=?, nombre=?  WHERE id_distri=? ");
            ps.setInt(1, distritoActualizado.getId_provincia());
            ps.setString(2, distritoActualizado.getNombre());
            ps.setInt(3, distritoActualizado.getId_distri());
            ps.executeUpdate();
            con.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        }
    }
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombre = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from distrito where id_distri=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombre = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombre;
    }
    
}
