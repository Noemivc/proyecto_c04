
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class modeloCurso {
    
     private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<claseCurso> obtenerCurso() throws Exception{
        
         List<claseCurso> cursos = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM curso");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_curso = rs.getInt("id_curso");
                String nombre = rs.getString("nombre");
                int nro_hora = rs.getInt("nro_hora");
                int nro_creditos = rs.getInt("nro_creditos");
                claseCurso cursoTemporal = new claseCurso(id_curso, nombre, nro_hora, nro_creditos);
                cursos.add(cursoTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return cursos;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    

    public void registrarCurso(claseCurso nuevoCurso) throws Exception 
    {
        
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO curso(nombre, nro_hora, nro_creditos) VALUES(?,?,?)");
            ps.setString(1, nuevoCurso.getNombre());
            ps.setInt(2, nuevoCurso.getNro_hora());
            ps.setInt(3, nuevoCurso.getNro_creditos());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

    public claseCurso cargarCurso(int id_curso) throws Exception
    {
       claseCurso cursoCargado = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM curso WHERE id_curso=?");
            ps.setInt(1, id_curso);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String nombre = rs.getString("nombre");
                int nro_hora  = rs.getInt("nro_hora");
                int nro_creditos  = rs.getInt("nro_creditos");
                cursoCargado = new claseCurso(id_curso, nombre, nro_hora, nro_creditos);
            }
            con.close();
            ps.close();
            rs.close();
            return cursoCargado;
        }
        catch (Exception e) 
        {
            throw e;
        }
   
    }

    public void actualizarCurso(claseCurso cursoActualizado) throws Exception
    {
       try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE curso SET nombre=?, nro_hora=?, nro_creditos=? WHERE id_curso=?");
            ps.setString(1, cursoActualizado.getNombre());
            ps.setInt(2, cursoActualizado.getNro_hora());
            ps.setInt(3, cursoActualizado.getNro_creditos());
            ps.setInt(4, cursoActualizado.getId_curso());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

    public void eliminarCurso(int id_curso) throws Exception
    {
       
         try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM curso WHERE id_curso=?");
            ps.setInt(1, id_curso);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

    public String obtenerNombre(int id) throws Exception
    {
        String nombreCC = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from curso where id_curso=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombreCC = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombreCC;
    }
    
}