package modelo;
public class clasedetallematricula {
 private int id_detalle;
 private int id_matricula;
 private int 	id_horario;

    public clasedetallematricula(int id_detalle, int id_matricula, int id_horario) {
        this.id_detalle = id_detalle;
        this.id_matricula = id_matricula;
        this.id_horario = id_horario;
    }

    public clasedetallematricula(int id_matricula, int id_horario) {
        this.id_matricula = id_matricula;
        this.id_horario = id_horario;
    }

    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public int getId_matricula() {
        return id_matricula;
    }

    public void setId_matricula(int id_matricula) {
        this.id_matricula = id_matricula;
    }

    public int getId_horario() {
        return id_horario;
    }

    public void setId_horario(int id_horario) {
        this.id_horario = id_horario;
    }
 
}
