
package modelo;

public class claseEstudiante 
{
     private int id_estudiante;
    private String dni;
    private String nombre;
    private String apellidos;
    private String genero;
    private String celular;
   private String correo;
    private String fecha_nac;
    private String lugar_nac;
    private int id_distrito;
    private String direccion;
    private String estado;

    public claseEstudiante(int id_estudiante, String dni, String nombre, String apellidos, String genero, String celular, String correo, String fecha_nac, String lugar_nac, int id_distrito, String direccion, String estado) {
        this.id_estudiante = id_estudiante;
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.genero = genero;
        this.celular = celular;
        this.correo = correo;
        this.fecha_nac = fecha_nac;
        this.lugar_nac = lugar_nac;
        this.id_distrito = id_distrito;
        this.direccion = direccion;
        this.estado = estado;
    }

    public claseEstudiante(String dni, String nombre, String apellidos, String genero, String celular, String correo, String fecha_nac, String lugar_nac, int id_distrito, String direccion, String estado) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.genero = genero;
        this.celular = celular;
        this.correo = correo;
        this.fecha_nac = fecha_nac;
        this.lugar_nac = lugar_nac;
        this.id_distrito = id_distrito;
        this.direccion = direccion;
        this.estado = estado;
    }

    public int getId_estudiante() {
        return id_estudiante;
    }

    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(String fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public String getLugar_nac() {
        return lugar_nac;
    }

    public void setLugar_nac(String lugar_nac) {
        this.lugar_nac = lugar_nac;
    }

    public int getId_distrito() {
        return id_distrito;
    }

    public void setId_distrito(int id_distrito) {
        this.id_distrito = id_distrito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

}
