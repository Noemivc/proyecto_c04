
package modelo;
/*
 * @author DAMARIS VILLALOBOS
 */
public class claseEmpleado 
{
    private int id_Empleado;
    private String dni;
    private String nombre;
    private String cargo;
    private String  Usuario;
    private String clave;
    private String estado;

    public claseEmpleado(int id_Empleado, String dni, String nombre, String cargo, String Usuario, String clave, String estado) {
        this.id_Empleado = id_Empleado;
        this.dni = dni;
        this.nombre = nombre;
        this.cargo = cargo;
        this.Usuario = Usuario;
        this.clave = clave;
        this.estado = estado;
    }

    public claseEmpleado(String dni, String nombre, String cargo, String Usuario, String clave, String estado) {
        this.dni = dni;
        this.nombre = nombre;
        this.cargo = cargo;
        this.Usuario = Usuario;
        this.clave = clave;
        this.estado = estado;
    }

    public int getId_Empleado() {
        return id_Empleado;
    }

    public void setId_Empleado(int id_Empleado) {
        this.id_Empleado = id_Empleado;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    
}
