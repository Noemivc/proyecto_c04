package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class modeloprovincia {
     private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
       public List<claseprovincia> obtenerProvincia() throws Exception
    {
        List<claseprovincia> provincias = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("SELECT * FROM provincia");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id = rs.getInt("id_provin");
                int id_depa = rs.getInt("id_departamento");
                String nombre = rs.getString("nombre");
                claseprovincia provinciaTemporal = new claseprovincia(id,id_depa,nombre);
                provincias.add(provinciaTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return provincias;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarProvincia(claseprovincia nuevaprovincia) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("INSERT INTO provincia(id_departamento,nombre) VALUES(?,?)");
            ps.setInt(1, nuevaprovincia.getId_departamento());
            ps.setString(2, nuevaprovincia.getNombre());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseprovincia cargarProvincia(int id) throws Exception
    {
        claseprovincia provinciaCargada = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("SELECT * FROM provincia WHERE id_provin=?");
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next())
            {
                int id_departamento = rs.getInt("id_departamento");
                String nombre = rs.getString("nombre");
             
                provinciaCargada = new claseprovincia(id_departamento,nombre);
            }
            con.close();
            ps.close();
            rs.close();
            return provinciaCargada;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarProvincia(claseprovincia provinciaActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("UPDATE provincia SET id_departamento=?, nombre=?  WHERE id_provin=?");
            ps.setInt(1, provinciaActualizado.getId_departamento());
            ps.setString(2, provinciaActualizado.getNombre());
            ps.setInt(3, provinciaActualizado.getId_provin());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombre = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from provincia where id_provin=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombre = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombre;
    }
 
}
