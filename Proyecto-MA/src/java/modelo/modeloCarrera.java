
package modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class modeloCarrera {
    
    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<claseCarrera> obtenerCarreras() throws Exception
    {
        List<claseCarrera> carreras = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM Carrera");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_Empleado = rs.getInt("id_carrera");
                String nombre = rs.getString("nombre");
                claseCarrera carreraTemporal = new claseCarrera(id_Empleado,nombre);
                carreras.add(carreraTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return carreras;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarCarrera(claseCarrera nuevaCarrera) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO Carrera(nombre) VALUES(?)");
            ps.setString(1, nuevaCarrera.getNombre());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseCarrera cargarCarrera(int id_carrera) throws Exception
    {
        claseCarrera carreraCargada = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM Carrera WHERE id_carrera=?");
            ps.setInt(1, id_carrera);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String nombre = rs.getString("nombre");
                carreraCargada = new claseCarrera(id_carrera, nombre);
            }
            con.close();
            ps.close();
            rs.close();
            return carreraCargada;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarCarrera(claseCarrera carreraActualizada) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE Carrera SET nombre=?  WHERE id_carrera=?");
            ps.setString(1, carreraActualizada.getNombre());
            ps.setInt(2, carreraActualizada.getId_carrera());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void eliminarCarrera(int id_carrera) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM Carrera WHERE id_carrera=?");
            ps.setInt(1, id_carrera);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombreC = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from carrera where id_carrera=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombreC = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombreC;
    }
    
}
