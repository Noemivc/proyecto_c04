
package modelo;

/**
 *
 * @author DAMARIS VILLALOBOS
 */
public class claseAsistencia 
{
    private int id_asistencia;
    private int id_horario;	
    private int id_estudiante;
    private String fecha;	
    private String tipo;

    public claseAsistencia(int id_asistencia, int id_horario, int id_estudiante, String fecha, String tipo) {
        this.id_asistencia = id_asistencia;
        this.id_horario = id_horario;
        this.id_estudiante = id_estudiante;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public claseAsistencia(int id_horario, int id_estudiante, String fecha, String tipo) {
        this.id_horario = id_horario;
        this.id_estudiante = id_estudiante;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public int getId_asistencia() {
        return id_asistencia;
    }

    public void setId_asistencia(int id_asistencia) {
        this.id_asistencia = id_asistencia;
    }

    public int getId_horario() {
        return id_horario;
    }

    public void setId_horario(int id_horario) {
        this.id_horario = id_horario;
    }

    public int getId_estudiante() {
        return id_estudiante;
    }

    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    

}
