
package modelo;

public class claseAula 
{
    private int id_aula;
    private String tipoAula;
    private int numero;
    private int piso;

    public claseAula(int id_aula, String tipoAula, int numero, int piso) {
        this.id_aula = id_aula;
        this.tipoAula = tipoAula;
        this.numero = numero;
        this.piso = piso;
    }

    public claseAula(String tipoAula, int numero, int piso) {
        this.tipoAula = tipoAula;
        this.numero = numero;
        this.piso = piso;
    }

    public int getId_aula() {
        return id_aula;
    }

    public void setId_aula(int id_aula) {
        this.id_aula = id_aula;
    }

    public String getTipoAula() {
        return tipoAula;
    }

    public void setTipoAula(String tipoAula) {
        this.tipoAula = tipoAula;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }
    
    

}
