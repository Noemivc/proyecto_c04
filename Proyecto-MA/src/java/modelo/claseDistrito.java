
package modelo;

public class claseDistrito 
{
    
    private int id_distri;
    private int id_provincia;
    private String nombre;

    public claseDistrito(int id_distri, int id_provincia, String nombre) {
        this.id_distri = id_distri;
        this.id_provincia = id_provincia;
        this.nombre = nombre;
    }

    public claseDistrito(int id_provincia, String nombre) {
        this.id_provincia = id_provincia;
        this.nombre = nombre;
    }

    public int getId_distri() {
        return id_distri;
    }

    public void setId_distri(int id_distri) {
        this.id_distri = id_distri;
    }

    public int getId_provincia() {
        return id_provincia;
    }

    public void setId_provincia(int id_provincia) {
        this.id_provincia = id_provincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
