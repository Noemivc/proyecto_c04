
package modelo;

public class claseHorario 
{
    private int id_horario;
    private int id_estudiante;
    private int id_carrera;
    private int id_cursos; 
    private int id_docente;
    private int id_aula; 
    private String dia;
    private String horainicio;
    private String horafin;

    public claseHorario(int id_horario, int id_estudiante, int id_carrera, int id_cursos, int id_docente, int id_aula, String dia, String horainicio, String horafin) {
        this.id_horario = id_horario;
        this.id_estudiante = id_estudiante;
        this.id_carrera = id_carrera;
        this.id_cursos = id_cursos;
        this.id_docente = id_docente;
        this.id_aula = id_aula;
        this.dia = dia;
        this.horainicio = horainicio;
        this.horafin = horafin;
    }

    public claseHorario(int id_estudiante, int id_carrera, int id_cursos, int id_docente, int id_aula, String dia, String horainicio, String horafin) {
        this.id_estudiante = id_estudiante;
        this.id_carrera = id_carrera;
        this.id_cursos = id_cursos;
        this.id_docente = id_docente;
        this.id_aula = id_aula;
        this.dia = dia;
        this.horainicio = horainicio;
        this.horafin = horafin;
    }

    public int getId_horario() {
        return id_horario;
    }

    public void setId_horario(int id_horario) {
        this.id_horario = id_horario;
    }

    public int getId_estudiante() {
        return id_estudiante;
    }

    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public int getId_carrera() {
        return id_carrera;
    }

    public void setId_carrera(int id_carrera) {
        this.id_carrera = id_carrera;
    }

    public int getId_cursos() {
        return id_cursos;
    }

    public void setId_cursos(int id_cursos) {
        this.id_cursos = id_cursos;
    }

    public int getId_docente() {
        return id_docente;
    }

    public void setId_docente(int id_docente) {
        this.id_docente = id_docente;
    }

    public int getId_aula() {
        return id_aula;
    }

    public void setId_aula(int id_aula) {
        this.id_aula = id_aula;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(String horainicio) {
        this.horainicio = horainicio;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    
    
}
