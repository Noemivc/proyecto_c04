package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class modelodetallematricula {
  private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<clasedetallematricula> obtenerDetalle() throws Exception
    {
        List<clasedetallematricula> detallematricula = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM detallematricula");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_matricula = rs.getInt("id_matricula");
                int id_horario = rs.getInt("id_horario");
               clasedetallematricula detalleTemporal = new clasedetallematricula(id_matricula,id_horario);
                detallematricula.add(detalleTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return detallematricula;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarDetallematricula(claseEmpleado nuevoEmpleado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO Empleado(dni, nombre, cargo, Usuario, clave, estado) VALUES(?,?,?,?,?,?)");
            ps.setString(1, nuevoEmpleado.getDni());
            ps.setString(2, nuevoEmpleado.getNombre());
            ps.setString(3, nuevoEmpleado.getCargo());
            ps.setString(4, nuevoEmpleado.getUsuario());
            ps.setString(5, nuevoEmpleado.getClave());
            ps.setString(6, nuevoEmpleado.getEstado());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseEmpleado cargarDetallematricula(int id_Empleado) throws Exception
    {
        claseEmpleado empleadoCargado = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM Empleado WHERE id_Empleado=?");
            ps.setInt(1, id_Empleado);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String dni = rs.getString("dni");
                String nombre = rs.getString("nombre");
                String cargo = rs.getString("cargo");
                String Usuario = rs.getString("Usuario");
                String clave = rs.getString("clave");
                String estado = rs.getString("estado");
                empleadoCargado = new claseEmpleado(id_Empleado, dni, nombre, cargo, Usuario, clave, estado);
            }
            con.close();
            ps.close();
            rs.close();
            return empleadoCargado;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarDetallematricula(claseEmpleado empleadoActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE Empleado SET dni=?, nombre=?, cargo=?, Usuario=?, clave=?, estado=? WHERE id_Empleado=?");
            ps.setString(1, empleadoActualizado.getDni());
            ps.setString(2, empleadoActualizado.getNombre());
            ps.setString(3, empleadoActualizado.getCargo());
            ps.setString(4, empleadoActualizado.getUsuario());
            ps.setString(5, empleadoActualizado.getClave());
            ps.setString(6, empleadoActualizado.getEstado());
            ps.setInt(7, empleadoActualizado.getId_Empleado());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void eliminarDetallematricula(int id_Empleado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM Empleado WHERE id_Empleado=?");
            ps.setInt(1, id_Empleado);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }   
}
