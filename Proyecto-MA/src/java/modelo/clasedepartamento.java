package modelo;
public class clasedepartamento {
       private int iddep;
       private String nombre;

    public clasedepartamento(int iddep, String nombre) {
        this.iddep = iddep;
        this.nombre = nombre;
    }

    public clasedepartamento(String nombre) {
        this.nombre = nombre;
    }

    public int getIddep() {
        return iddep;
    }

    public void setIddep(int iddep) {
        this.iddep = iddep;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
