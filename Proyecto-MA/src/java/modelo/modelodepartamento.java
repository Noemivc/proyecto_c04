package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class modelodepartamento {
       private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
       public List<clasedepartamento> obtenerDepartamentos() throws Exception
    {
        List<clasedepartamento> departamentos = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("SELECT *FROM departamento");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id = rs.getInt("id_depar");
                String nombre = rs.getString("nombre");
                clasedepartamento departamentoTemporal = new clasedepartamento(id,nombre);
                departamentos.add(departamentoTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return departamentos;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarDepartamento(clasedepartamento nuevodepartamento) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("INSERT INTO departamento(nombre) VALUES(?)");
            ps.setString(1, nuevodepartamento.getNombre());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public clasedepartamento cargarDepartamento(int id) throws Exception
    {
        clasedepartamento departamentoCargado = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
          
            ps = con.prepareCall("SELECT * FROM departamento WHERE id_depar=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String nombre = rs.getString("nombre");
             
                departamentoCargado = new clasedepartamento(id,nombre);
            }
            con.close();
            ps.close();
            rs.close();
            return departamentoCargado;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarDepartamento(clasedepartamento departamentoActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            ps = con.prepareCall("UPDATE departamento SET nombre=?  WHERE id_depar=?");
            ps.setString(1, departamentoActualizado.getNombre());
            ps.setInt(2, departamentoActualizado.getIddep());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombre = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select nombre from departamento where id_depar=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombre = rs.getString("nombre");
        }
        con.close();
        ps.close();
        rs.close();
        return nombre;
    }
}
