
package modelo;

    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.util.ArrayList;
    import java.util.List;

public class modeloAsistencia 
{
    
    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    
    public List<claseAsistencia> obtenerAsistencia() throws Exception
    {
        List<claseAsistencia> asistencia = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM asistencia");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_asistencia = rs.getInt("id_asistencia");
                int id_horario = rs.getInt("id_horario");
                int id_estudiante = rs.getInt("id_estudiante");
                String fecha = rs.getString("fecha");
                String tipo = rs.getString("tipo");
                claseAsistencia asistenciaTemporal = new claseAsistencia(id_asistencia,id_horario, id_estudiante, fecha, tipo);
                asistencia.add(asistenciaTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return asistencia;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarAsistencia(claseAsistencia nuevoAsistencia) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");

            ps = con.prepareCall("INSERT INTO asistencia (id_horario, id_estudiante,  fecha, tipo) VALUES(?,?,?,?)");
            ps.setInt(1, nuevoAsistencia.getId_horario()); 
            ps.setInt(2, nuevoAsistencia.getId_estudiante());                     
            ps.setString(3, nuevoAsistencia.getFecha());
            ps.setString(4, nuevoAsistencia.getTipo());
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseAsistencia cargarAsistencia (int id) throws Exception
    {
        claseAsistencia asistenciaCargada = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");

            ps = con.prepareCall("SELECT * FROM asistencia WHERE id_asistencia=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next())
            {
                int id_horario = rs.getInt("id_horario");
                int id_estudiante = rs.getInt("id_estudiante");
                String fecha = rs.getString("fecha");
                String tipo = rs.getString("tipo");
                asistenciaCargada = new claseAsistencia(id, id_horario, id_estudiante, fecha, tipo);
            }
            con.close();
            ps.close();
            rs.close();
            return asistenciaCargada;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarAsistencia(claseAsistencia asistenciaActualizado) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");

            ps = con.prepareCall("UPDATE asistencia SET id_horario=?, id_estudiante=?, fecha=?, tipo=? WHERE id_asistencia=? ");
            ps.setInt(1, asistenciaActualizado.getId_horario()); 
            ps.setInt(2, asistenciaActualizado.getId_estudiante());                     
            ps.setString(3, asistenciaActualizado.getFecha());
            ps.setString(4, asistenciaActualizado.getTipo());
            ps.setInt(5, asistenciaActualizado.getId_asistencia());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (ClassNotFoundException | SQLException e) 
        {
            throw e;
        }
    }
    
    public void eliminarAsistencia(int id) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");

            ps = con.prepareCall("DELETE FROM asistencia WHERE id_asistencia=?");
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
}
