
package modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
 * @author DAMARIS VILLALOBOS
 */
public class modeloAula {
    
    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public List<claseAula> obtenerAula() throws Exception
    {
        List<claseAula> aulas = new ArrayList<>();
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM Aula");
            rs = ps.executeQuery();
            while(rs.next())
            {
                int id_aula = rs.getInt("id_aula");
                String tipoAula = rs.getString("tipoAula");
                int numero = rs.getInt("numero");
                int piso = rs.getInt("piso");
                claseAula aulaTemporal = new claseAula(id_aula, tipoAula, numero, piso);
                aulas.add(aulaTemporal);
            }
            con.close();
            ps.close();
            rs.close();
            return aulas;
        } 
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public void registrarAula(claseAula nuevaAula) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "INSERT INTO productos(categoria, nombre, precio, stock, paisOrigen) VALUES(?,?,?,?,?)";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("INSERT INTO Aula(tipoAula,numero,piso) VALUES(?,?,?)");
            ps.setString(1, nuevaAula.getTipoAula());
            ps.setInt(2, nuevaAula.getNumero());
            ps.setInt(3, nuevaAula.getPiso());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public claseAula cargarAula(int id_aula) throws Exception
    {
        claseAula aulaCargada = null;
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "SELECT * FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("SELECT * FROM Aula WHERE id_aula=?");
            ps.setInt(1, id_aula);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String tipoAula = rs.getString("tipoAula");
                int numero = rs.getInt("numero");
                int piso = rs.getInt("piso");
                aulaCargada = new claseAula(id_aula,tipoAula,numero,piso);
            }
            con.close();
            ps.close();
            rs.close();
            return aulaCargada;
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void actualizarAula(claseAula aulaActualizada) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "UPDATE productos SET categoria=?, nombre=?, precio=?, stock=?, paisOrigen=? WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("UPDATE Aula SET tipoAula=?,numero=?,piso=? WHERE id_aula=?");
            ps.setString(1, aulaActualizada.getTipoAula());
            ps.setInt(2, aulaActualizada.getNumero());
            ps.setInt(3, aulaActualizada.getPiso());
            ps.setInt(4, aulaActualizada.getId_aula());
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    public void eliminarAula(int id_aula) throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
            //String sql = "DELETE FROM productos WHERE id=?";
            //ps = con.prepareStatement(sql);
            ps = con.prepareCall("DELETE FROM Aula WHERE id_aula=?");
            ps.setInt(1, id_aula);
            ps.executeUpdate();
            con.close();
            ps.close();
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    
    public String obtenerNombre(int id) throws Exception
    {
        String nombreA = "";
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_datosidat","root","");
        ps = con.prepareCall("select numero from aula where id_aula=?");
        ps.setInt(1, id);
        rs = ps.executeQuery();
        if(rs.next())
        {
            nombreA = rs.getString("numero");
        }
        con.close();
        ps.close();
        rs.close();
        return nombreA;
    }
}
