
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseCarrera;
import modelo.modeloCarrera;

/*
 * @author DAMARIS VILLALOBOS
 */
@WebServlet(name = "controladorCarrera", urlPatterns = {"/controladorCarrera"})
public class controladorCarrera extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarCarreras(request, response);
                break;
            case "Registrar":
                registrarCarrera(request, response);
                break;
            case "Cargar":
                cargarCarrera(request, response);
                break;
            case "Actualizar":
                actualizarCarrera(request, response);
                break;
            case "Eliminar":
                eliminarCarrera(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
    
    public void listarCarreras(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseCarrera> carreras;
            modeloCarrera mP = new modeloCarrera();
            carreras = mP.obtenerCarreras();
            request.setAttribute("carreras", carreras);
            request.getRequestDispatcher("/vistas/carrera.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarCarrera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {

            String nombre = request.getParameter("txtnombre");
            
            claseCarrera nuevaCarrera = new claseCarrera(nombre);
            modeloCarrera mP = new modeloCarrera();
            mP.registrarCarrera(nuevaCarrera);
            response.sendRedirect("controladorCarrera");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarCarrera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_carre = Integer.parseInt(request.getParameter("id_carre"));
            modeloCarrera mP = new modeloCarrera();
            claseCarrera carreraCargada = mP.cargarCarrera(id_carre);
            request.setAttribute("carreraCargada", carreraCargada);
            request.getRequestDispatcher("/vistas/actualizarCarrera.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarCarrera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_carre = Integer.parseInt(request.getParameter("id_carre"));
            String nombre = request.getParameter("txtnombre");
            
            claseCarrera carreraActualizada = new claseCarrera(id_carre, nombre);
            modeloCarrera mP = new modeloCarrera();
            mP.actualizarCarrera(carreraActualizada);
            response.sendRedirect("controladorCarrera");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarCarrera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_carre = Integer.parseInt(request.getParameter("id_carre"));
            modeloCarrera mP = new modeloCarrera();
            mP.eliminarCarrera(id_carre);
            response.sendRedirect("controladorCarrera");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

}
