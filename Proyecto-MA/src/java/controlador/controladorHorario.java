
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseEmpleado;
import modelo.claseEstudiante;
import modelo.claseHorario;
import modelo.modeloEmpleado;
import modelo.modeloEstudiante;
import modelo.modeloHorario;

/*
 * @author DAMARIS VILLALOBOS
 */
@WebServlet(name = "controladorHorario", urlPatterns = {"/controladorHorario"})
public class controladorHorario extends HttpServlet
{

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarHorario(request, response);
                break;
            case "Registrar":
                registrarHorario(request, response);
                break;
            case "Cargar":
                cargarHorario(request, response);
                break;
            case "Actualizar":
                actualizarHorario(request, response);
                break;
            case "Eliminar":
                eliminarHorario(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        
    }
    
    public void listarHorario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseHorario> horario;
            modeloHorario mE = new modeloHorario();
            horario = mE.obtenerHorario();
            request.setAttribute("horario", horario);
            listarEmpleados(request);
            listarEstudiantes(request);
            request.getRequestDispatcher("/vistas/listarHorario.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarHorario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {  
            
            int id_carrera = Integer.parseInt(request.getParameter("txtid_carrera"));
            int id_estudiante = Integer.parseInt(request.getParameter("txtid_estudiante"));
            int id_cursos = Integer.parseInt(request.getParameter("txtid_cursos"));
            int id_docente = Integer.parseInt(request.getParameter("txtid_docente"));
            int id_aula = Integer.parseInt(request.getParameter("txtid_aula"));
            String dia = request.getParameter("txtdia");
            String horainicio = request.getParameter("txthorainicio");
            String horafin = request.getParameter("txthorafin");
            claseHorario nuevoHorario = new claseHorario(id_carrera,id_estudiante, id_cursos, id_docente, id_aula, dia, horainicio, horafin);
            modeloHorario mE = new modeloHorario();
            mE.registrarHorario(nuevoHorario);
            response.sendRedirect("controladorHorario");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarHorario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_horario = Integer.parseInt(request.getParameter("id_horario"));
            modeloHorario mE = new modeloHorario();
            claseHorario horarioCargado = mE.cargarHorario(id_horario);
            request.setAttribute("horarioCargado",horarioCargado);
            listarEmpleados(request);
            listarEstudiantes(request);
            request.getRequestDispatcher("/vistas/actualizarHorario.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarHorario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_horario = Integer.parseInt(request.getParameter("id_horario"));
            int id_estudiante = Integer.parseInt(request.getParameter("txtid_estudiante"));
            int id_carrera = Integer.parseInt(request.getParameter("txtid_carrera"));
            int id_cursos = Integer.parseInt(request.getParameter("txtid_cursos"));
            int id_docente = Integer.parseInt(request.getParameter("txtid_docente"));
            int id_aula = Integer.parseInt(request.getParameter("txtid_aula"));
            String dia = request.getParameter("txtdia");
            String horainicio = request.getParameter("txthorainicio");
            String horafin = request.getParameter("txthorafin");
            
            claseHorario horarioActualizado = new claseHorario(id_horario,id_estudiante, id_carrera, id_cursos, id_docente, id_aula, dia, horainicio, horafin);
            modeloHorario mE = new modeloHorario();
            mE.actualizarHorario(horarioActualizado);
            response.sendRedirect("controladorHorario");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarHorario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_horario = Integer.parseInt(request.getParameter("id_horario"));
            modeloHorario mP = new modeloHorario();
            mP.eliminarHorario(id_horario);
            response.sendRedirect("controladorHorario");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    
    public void listarEstudiantes(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseEstudiante> estudiante;
            modeloEstudiante md = new modeloEstudiante();
            estudiante = md.obtenerEstudiante();
            request.setAttribute("estudiante", estudiante);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    
    public void listarEmpleados(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseEmpleado> empleado;
            modeloEmpleado md = new modeloEmpleado();
            empleado = md.obtenerEmpleados();
            request.setAttribute("empleado", empleado);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

}
