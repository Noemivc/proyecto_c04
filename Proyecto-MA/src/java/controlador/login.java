
package controlador;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.modeloLogin;

/*
 * @author DAMARIS VILLALOBOS
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        validacion(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        validacion(request, response);
    }
    

    public void validacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            String usuario = request.getParameter("txtcorreo");
            String contraseña = request.getParameter("txtpass");
            
            

            modeloLogin ml = new modeloLogin();
            if (ml.autenticacion(usuario, contraseña)) {
                response.sendRedirect("Mantenimiento.html");
            } else {
                response.sendRedirect("login-Empleado.html");
            }

        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
   
    

}
