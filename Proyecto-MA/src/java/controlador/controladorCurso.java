
package controlador;

import modelo.claseCurso;
import modelo.modeloCurso;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "controladorCurso", urlPatterns = {"/controladorCurso"})
public class controladorCurso extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarCurso(request, response);
                break;
            case "Registrar":
                registrarCurso(request, response);
                break;
            case "Cargar":
                cargarCurso(request, response);
                break;
            case "Actualizar":
                actualizarCurso(request, response);
                break;
            case "Eliminar":
                eliminarCurso(request, response);
                break;
        }
    }

    public controladorCurso() {
    }

    public void listarCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseCurso> cursos;
            modeloCurso mC = new modeloCurso();
            cursos = mC.obtenerCurso();
            request.setAttribute("cursos", cursos);
            request.getRequestDispatcher("/vistas/listarCurso.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    public void registrarCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            String nombre = request.getParameter("txtNombre");
            int nro_hora = Integer.parseInt(request.getParameter("txtHora"));
            int nro_creditos = Integer.parseInt(request.getParameter("txtCreditos"));
            claseCurso nuevoCurso = new claseCurso(nombre, nro_hora, nro_creditos);
            modeloCurso mC = new modeloCurso();
            mC.registrarCurso(nuevoCurso);
            response.sendRedirect("controladorCurso");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
     public void cargarCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_curso = Integer.parseInt(request.getParameter("id_curso"));
            modeloCurso mC = new modeloCurso();
            claseCurso cursoCargado = mC.cargarCurso(id_curso);
            request.setAttribute("cursoCargado", cursoCargado);
            request.getRequestDispatcher("/vistas/actualizarCurso.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
     public void actualizarCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_curso = Integer.parseInt(request.getParameter("id_curso"));
            String nombre = request.getParameter("txtNombre");
            int nro_hora = Integer.parseInt(request.getParameter("txtHora"));
            int nro_creditos = Integer.parseInt(request.getParameter("txtCreditos"));
            claseCurso cursoActualizado = new claseCurso(id_curso, nombre, nro_hora, nro_creditos);
            modeloCurso mC = new modeloCurso();
            mC.actualizarCurso(cursoActualizado);
            response.sendRedirect("controladorCurso");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
      public void eliminarCurso(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_curso = Integer.parseInt(request.getParameter("id_curso"));
            modeloCurso mC = new modeloCurso();
            mC.eliminarCurso(id_curso);
            response.sendRedirect("controladorCurso");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

}