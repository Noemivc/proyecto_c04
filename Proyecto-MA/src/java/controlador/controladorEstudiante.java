
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseDistrito;
import modelo.claseEstudiante;
import modelo.modeloDistrito;
import modelo.modeloEstudiante;

/**
 *
 * @author LUIS
 */
@WebServlet(name = "controladorEstudiante", urlPatterns = {"/controladorEstudiante"})
public class controladorEstudiante extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarEstudiante(request, response);
                break;
            case "Registrar":
                registrarEstudiante(request, response);
                break;
            case "Cargar":
                cargarEstudiante(request, response);
                break;
            case "Actualizar":
                actualizarEstudiante(request, response);
                break;
            case "Eliminar":
                eliminarEstudiante(request, response);
                break;
        }
    }
    
    public void listarEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseEstudiante> estudiante;
            modeloEstudiante mE = new modeloEstudiante();
            estudiante = mE.obtenerEstudiante();
            request.setAttribute("estudiante", estudiante);
            listarDistritos(request);
            request.getRequestDispatcher("/vistas/listarEstudiante.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            String dni = request.getParameter("txtDni");
            String nombre = request.getParameter("txtNombre");
            String apellidos = request.getParameter("txtApellidos");
            String genero = request.getParameter("txtGenero");
            String celular = request.getParameter("txtCelular");
            String correo = request.getParameter("txtCorreo");
            String fecha_nac = request.getParameter("txtFecha_nac");
            String lugar_nac = request.getParameter("txtLugar_nac");
            int id_distrito = Integer.parseInt(request.getParameter("id_distrito"));
            String direccion = request.getParameter("txtDireccion");
            String estado = request.getParameter("txtEstado");
            claseEstudiante nuevoEstudiante = new claseEstudiante(dni, nombre, apellidos, genero, celular, correo, fecha_nac, lugar_nac, id_distrito, direccion, estado);
            modeloEstudiante mE = new modeloEstudiante();
            mE.registrarEstudiante(nuevoEstudiante);
            response.sendRedirect("controladorEstudiante");           
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_estudiante = Integer.parseInt(request.getParameter("id_estudiante"));
            modeloEstudiante mE = new modeloEstudiante();
            claseEstudiante estudianteCargado = mE.cargarEstudiante(id_estudiante);
            request.setAttribute("estudianteCargado",estudianteCargado);
            listarDistritos(request);
            request.getRequestDispatcher("/vistas/actualizarEstudiante.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_estudiante = Integer.parseInt(request.getParameter("id_estudiante"));
            String dni = request.getParameter("txtDni");
            String nombre = request.getParameter("txtNombre");
            String apellidos = request.getParameter("txtApellidos");
             String genero = request.getParameter("txtGenero");
            String celular = request.getParameter("txtCelular");
            String correo = request.getParameter("txtCorreo");
            String fecha_nac = request.getParameter("txtFecha_nac");
            String lugar_nac = request.getParameter("txtLugar_nac");
            int id_distrito = Integer.parseInt(request.getParameter("id_distrito"));
            String direccion = request.getParameter("txtDireccion");
            String estado = request.getParameter("txtEstado");
            claseEstudiante EstudianteActualizado = new claseEstudiante(id_estudiante, dni, nombre, apellidos, genero, 
                    celular,correo,fecha_nac,lugar_nac,id_distrito,direccion,estado);
            modeloEstudiante mE = new modeloEstudiante();
            mE.actualizarEstudiante(EstudianteActualizado);
            response.sendRedirect("controladorEstudiante");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarEstudiante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_estudiante = Integer.parseInt(request.getParameter("id_estudiante"));
            modeloEstudiante mP = new modeloEstudiante();
            mP.eliminarEstudiante(id_estudiante);
            response.sendRedirect("controladorEstudiante");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void listarDistritos(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseDistrito> distritos;
            modeloDistrito md = new modeloDistrito();
            distritos = md.obtenerDistrito();
            request.setAttribute("distritos", distritos);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
}
    
    
    
