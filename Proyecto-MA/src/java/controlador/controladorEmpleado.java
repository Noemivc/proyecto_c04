
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseEmpleado;
import modelo.modeloEmpleado;

@WebServlet(name = "controladorEmpleado", urlPatterns = {"/controladorEmpleado"})
public class controladorEmpleado extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarEmpleados(request, response);
                break;
            case "Registrar":
                registrarEmpleado(request, response);
                break;
            case "Cargar":
                cargarEmpleado(request, response);
                break;
            case "Actualizar":
                actualizarEmpleado(request, response);
                break;
            case "Eliminar":
                eliminarEmpleado(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
    public void listarEmpleados(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseEmpleado> empleados;
            modeloEmpleado mP = new modeloEmpleado();
            empleados = mP.obtenerEmpleados();
            request.setAttribute("empleados", empleados);
            request.getRequestDispatcher("/vistas/empleado.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            String dni = request.getParameter("txtdni");
            String nombre = request.getParameter("txtnombre");
            String cargo = request.getParameter("txtcargo");
            String Usuario = request.getParameter("txtusu");
            String clave = request.getParameter("txtclave");
            String estado = request.getParameter("txtestado");
            
            claseEmpleado nuevoEmpleado = new claseEmpleado(dni, nombre, cargo, Usuario, clave, estado);
            modeloEmpleado mP = new modeloEmpleado();
            mP.registrarEmpleado(nuevoEmpleado);
            response.sendRedirect("controladorEmpleado");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_emple = Integer.parseInt(request.getParameter("id_emple"));
            modeloEmpleado mP = new modeloEmpleado();
            claseEmpleado empleadoCargado = mP.cargarEmpleado(id_emple);
            request.setAttribute("empleadoCargado", empleadoCargado);
            request.getRequestDispatcher("/vistas/actualizarEmpleado.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_emple = Integer.parseInt(request.getParameter("id_emple"));
            String dni = request.getParameter("txtdni");
            String nombre = request.getParameter("txtnombre");
            String cargo = request.getParameter("txtcargo");
            String Usuario = request.getParameter("txtusu");
            String clave = request.getParameter("txtclave");
            String estado = request.getParameter("txtestado");
            
            claseEmpleado empleadoActualizado = new claseEmpleado(id_emple, dni, nombre, cargo, Usuario, clave, estado);
            modeloEmpleado mP = new modeloEmpleado();
            mP.actualizarEmpleado(empleadoActualizado);
            response.sendRedirect("controladorEmpleado");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_emple = Integer.parseInt(request.getParameter("id_emple"));
            modeloEmpleado mP = new modeloEmpleado();
            mP.eliminarEmpleado(id_emple);
            response.sendRedirect("controladorEmpleado");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

}
