
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseAsistencia;
import modelo.modeloAsistencia;

/*
 * @author DAMARIS VILLALOBOS
 */
@WebServlet(name = "controladorAsistencia", urlPatterns = {"/controladorAsistencia"})
public class controladorAsistencia extends HttpServlet
{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarAsistencia(request, response);
                break;
            case "Registrar":
                registrarAsistencia(request, response);
                break;
            case "Cargar":
                cargarAsistencia(request, response);
                break;
            case "Actualizar":
                actualizarAsistencia(request, response);
                break;
            case "Eliminar":
                eliminarAsistencia(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {

    }
    
    public void listarAsistencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseAsistencia> asistencia;
            modeloAsistencia mC = new modeloAsistencia();
            asistencia = mC.obtenerAsistencia();
            request.setAttribute("asistencia", asistencia);
            request.getRequestDispatcher("/vistas/listarAsistencia.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    public void registrarAsistencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {           
            int id_horario = Integer.parseInt(request.getParameter("txtid_horario"));
            int id_estudiante = Integer.parseInt(request.getParameter("txtid_estudiante"));
            String fecha = request.getParameter("txtfecha");
            String tipo = request.getParameter("txttipo");
            
            claseAsistencia nuevoAsistencia = new claseAsistencia(id_horario, id_estudiante, fecha, tipo);
            modeloAsistencia mC = new modeloAsistencia();
            mC.registrarAsistencia(nuevoAsistencia);
            response.sendRedirect("controladorAsistencia");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
     public void cargarAsistencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_asistencia = Integer.parseInt(request.getParameter("id_asistencia"));
            modeloAsistencia mC = new modeloAsistencia();
            claseAsistencia asistenciaCargado = mC.cargarAsistencia(id_asistencia);
            request.setAttribute("asistenciaCargado", asistenciaCargado);
            request.getRequestDispatcher("/vistas/actualizarAsistencia.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
     public void actualizarAsistencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_asistencia = Integer.parseInt(request.getParameter("id_asistencia"));
            int id_horario = Integer.parseInt(request.getParameter("txtid_horario"));
            int id_estudiante = Integer.parseInt(request.getParameter("txtid_estudiante"));
            String fecha = request.getParameter("txtfecha");
            String tipo = request.getParameter("txttipo");
            
            claseAsistencia asistenciaActualizado = new claseAsistencia(id_asistencia, id_horario, id_estudiante, fecha, tipo);
            modeloAsistencia mC = new modeloAsistencia();
            mC.actualizarAsistencia(asistenciaActualizado);
            response.sendRedirect("controladorAsistencia");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
      public void eliminarAsistencia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_asistencia = Integer.parseInt(request.getParameter("id_asistencia"));
            modeloAsistencia mC = new modeloAsistencia();
            mC.eliminarAsistencia(id_asistencia);
            response.sendRedirect("controladorAsistencia");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

}
