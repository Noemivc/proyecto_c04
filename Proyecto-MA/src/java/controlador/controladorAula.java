
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseAula;
import modelo.modeloAula;

/*
 * @author DAMARIS VILLALOBOS
 */
@WebServlet(name = "controladorAula", urlPatterns = {"/controladorAula"})
public class controladorAula extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
          String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarAula(request, response);
                break;
            case "Registrar":
                registrarAula(request, response);
                break;
            case "Cargar":
                cargarAula(request, response);
                break;
            case "Actualizar":
                actualizarAula(request, response);
                break;
            case "Eliminar":
                eliminarAula(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
    
     public void listarAula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<claseAula> aulas;
            modeloAula mP = new modeloAula();
            aulas = mP.obtenerAula();
            request.setAttribute("aulas", aulas);
            request.getRequestDispatcher("/vistas/aula.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarAula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {

            String tipoAula = request.getParameter("txtaula");
            int numero = Integer.parseInt(request.getParameter("txtnumero"));
            int piso = Integer.parseInt(request.getParameter("txtpiso"));
            claseAula nuevaAula = new claseAula(tipoAula, numero, piso);
            modeloAula mP = new modeloAula();
            mP.registrarAula(nuevaAula);
            response.sendRedirect("controladorAula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarAula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_aula = Integer.parseInt(request.getParameter("id_aula"));
            modeloAula mP = new modeloAula();
            claseAula aulaCargada = mP.cargarAula(id_aula);
            request.setAttribute("aulaCargada", aulaCargada);
            request.getRequestDispatcher("/vistas/actualizarAula.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarAula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_aula = Integer.parseInt(request.getParameter("id_aula"));
            String tipoAula = request.getParameter("txtaula");
            int numero = Integer.parseInt(request.getParameter("txtnumero"));
            int piso = Integer.parseInt(request.getParameter("txtpiso"));
            
            claseAula aulaActualizada = new claseAula(id_aula, tipoAula, numero, piso);
            modeloAula mP = new modeloAula();
            mP.actualizarAula(aulaActualizada);
            response.sendRedirect("controladorAula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarAula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_aula = Integer.parseInt(request.getParameter("id_aula"));
            modeloAula mP = new modeloAula();
            mP.eliminarAula(id_aula);
            response.sendRedirect("controladorAula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

}
