/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.clasedepartamento;
import modelo.modelodepartamento;

/**
 *
 * @author Lenovo
 */
@WebServlet(name = "controladordepartamento", urlPatterns = {"/controladordepartamento"})
public class controladordepartamento extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarDepartamento(request, response);
                break;
            case "Registrar":
                registrarDepartamento(request, response);
                break;
            case "Cargar":
                cargarDepartamento(request, response);
                break;
            case "Actualizar":
                actualizarDepartamento(request, response);
                break;
            
        }
        
    }

 public void listarDepartamento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
         try 
        {
            List<clasedepartamento> departamento;
            modelodepartamento mD = new modelodepartamento();
            departamento= mD.obtenerDepartamentos();
            request.setAttribute("departamento",departamento);
            request.getRequestDispatcher("vistas/listardepartamento.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
       
       }
        public void registrarDepartamento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            String nombre = request.getParameter("txtNombre");
            clasedepartamento nuevodepartamento = new clasedepartamento(nombre);
            modelodepartamento mD = new modelodepartamento();
            mD.registrarDepartamento(nuevodepartamento);
            response.sendRedirect("controladordepartamento");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
     public void cargarDepartamento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_depar = Integer.parseInt(request.getParameter("id_depar"));
            modelodepartamento mD = new modelodepartamento();
            clasedepartamento departamentoCargado = mD.cargarDepartamento(id_depar);
            request.setAttribute("departamentoCargado", departamentoCargado);
            request.getRequestDispatcher("vistas/actualizardepartamento.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarDepartamento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_depar = Integer.parseInt(request.getParameter("id_depar"));
            String nombre = request.getParameter("txtNombre");
            clasedepartamento departamentoActualizado = new clasedepartamento(id_depar,nombre);
            modelodepartamento mD = new modelodepartamento();
            mD.actualizarDepartamento(departamentoActualizado);
            response.sendRedirect("controladordepartamento");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
   
}   

