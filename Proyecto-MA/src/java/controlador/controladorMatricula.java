/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseEmpleado;
import modelo.claseEstudiante;
import modelo.clasematricula;
import modelo.modeloEmpleado;
import modelo.modeloEstudiante;
import modelo.modelomatricula;

/**
 *
 * @author Lenovo
 */
@WebServlet(name = "controladorMatricula", urlPatterns = {"/controladorMatricula"})
public class controladorMatricula extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarMatricula(request, response);
                break;
            case "Registrar":
                registrarMatricula(request, response);
                break;
            case "Cargar":
                cargarMatricula(request, response);
                break;
            case "Actualizar":
                actualizarMatricula(request, response);
                break;
            case "Eliminar":
                eliminarMatricula(request, response);
                break;
        }
        
    }
     public void listarMatricula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            List<clasematricula> matricula;
            modelomatricula mM = new modelomatricula();
            matricula = mM.obtenerMatricula();
            request.setAttribute("matricula", matricula);
            listarEmpleados(request);
            listarEstudiantes(request);
            request.getRequestDispatcher("/vistas/listarmatricula.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void registrarMatricula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_estudiantes = Integer.parseInt(request.getParameter("id_estudiantes"));
            String fecha = request.getParameter("txtFecha");
            String ciclo = request.getParameter("txtCiclo");
            String condiciones = request.getParameter("txtCondiciones");
            int id_Empleado = Integer.parseInt(request.getParameter("id_Empleado"));
            clasematricula nuevamatricula = new clasematricula(id_estudiantes, fecha, ciclo, condiciones, id_Empleado);
            modelomatricula mM = new modelomatricula();
            mM.registrarMatricula(nuevamatricula);
            response.sendRedirect("controladorMatricula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void cargarMatricula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_matricula = Integer.parseInt(request.getParameter("id_matricula"));
            modelomatricula mM = new modelomatricula();
            clasematricula matriculaCargada = mM.cargarMatricula(id_matricula);
            request.setAttribute("matriculaCargada",matriculaCargada);
            listarEmpleados(request);
            listarEstudiantes(request);
            request.getRequestDispatcher("/vistas/actualizarMatricula.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarMatricula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_matricula = Integer.parseInt(request.getParameter("id_matricula"));
            String fecha = request.getParameter("txtFecha");
            String ciclo = request.getParameter("txtCiclo");
            String condiciones = request.getParameter("txtCondiciones");
            int id_Empleado = Integer.parseInt(request.getParameter("id_Empleado"));
            clasematricula matriculaActualizada = new clasematricula(id_matricula, fecha, ciclo, condiciones, id_Empleado );
            modelomatricula mM = new modelomatricula();
            mM.actualizarMatricula(matriculaActualizada);
            response.sendRedirect("controladorMatricula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void eliminarMatricula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try 
        {
            int id_matricula = Integer.parseInt(request.getParameter("id_matricula"));
            modelomatricula mM = new modelomatricula();
            mM.eliminarMatricula(id_matricula);
            response.sendRedirect("controladorMatricula");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }
    
    public void listarEstudiantes(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseEstudiante> estudiante;
            modeloEstudiante md = new modeloEstudiante();
            estudiante = md.obtenerEstudiante();
            request.setAttribute("estudiante", estudiante);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    
    
    public void listarEmpleados(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseEmpleado> empleado;
            modeloEmpleado md = new modeloEmpleado();
            empleado = md.obtenerEmpleados();
            request.setAttribute("empleado", empleado);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

}
