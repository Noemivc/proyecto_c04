
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.claseDistrito;
import modelo.claseprovincia;
import modelo.modeloDistrito;
import modelo.modeloprovincia;

@WebServlet(name = "controladorDistrito", urlPatterns = {"/controladorDistrito"})
public class controladorDistrito extends HttpServlet 
{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String accion = request.getParameter("accion");
        
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarDistrito(request, response);
                break;
            case "Registrar":
                registrarDistrito(request, response);
                break;
            case "Cargar":
                cargarDistrito(request, response);
                break;
            case "Actualizar":
                actualizarDistrito(request, response);
                break;
            
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        
    }

    public void listarDistrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<claseDistrito> distrito;
            modeloDistrito mD = new modeloDistrito();
            distrito = mD.obtenerDistrito();
            request.setAttribute("distrito", distrito);
            listarProvincias(request);
            request.getRequestDispatcher("/vistas/listarDistrito.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }

    }

    public void registrarDistrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id_provincia = Integer.parseInt(request.getParameter("id_provincia"));
            String nombre = request.getParameter("txtnombre");
            claseDistrito nuevodistrito = new claseDistrito(id_provincia, nombre);
            modeloDistrito mD = new modeloDistrito();
            mD.registrarDistrito(nuevodistrito);
            response.sendRedirect("controladorDistrito");
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/vistas/error.jsp").forward(request, response);
        }
    }

    public void cargarDistrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id_distri = Integer.parseInt(request.getParameter("id_distri"));
            modeloDistrito mD = new modeloDistrito();
            claseDistrito distritoCargado = mD.cargarDistrito(id_distri);
            request.setAttribute("distritoCargado", distritoCargado);
            listarProvincias(request);
            request.getRequestDispatcher("vistas/actualizarDistrito.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }

    public void actualizarDistrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id_distri = Integer.parseInt(request.getParameter("id_distri"));
            int id_provincia = Integer.parseInt(request.getParameter("id_provincia"));
            String nombre = request.getParameter("txtnombre");
            claseDistrito distritoActualizado = new claseDistrito(id_distri, id_provincia, nombre);
            modeloDistrito mD = new modeloDistrito();
            mD.actualizarDistrito(distritoActualizado);
            response.sendRedirect("controladorDistrito");
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
    
    public void listarProvincias(HttpServletRequest request) throws Exception {
        try 
        {
            List<claseprovincia> provincias;
            modeloprovincia md = new modeloprovincia();
            provincias = md.obtenerProvincia();
            request.setAttribute("provincias", provincias);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

}
