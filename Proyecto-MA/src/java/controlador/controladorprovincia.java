/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.clasedepartamento;
import modelo.claseprovincia;
import modelo.modelodepartamento;
import modelo.modeloprovincia;


/**
 *
 * @author Lenovo
 */
@WebServlet(name = "controladorprovincia", urlPatterns = {"/controladorprovincia"})
public class controladorprovincia extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
        String accion = request.getParameter("accion");
        if(accion==null)
        {
            accion = "Listar";
        }
        switch(accion)
        {
            case "Listar":
                listarProvincia(request, response);
                break;
            case "Registrar":
                registrarProvincia(request, response);
                break;
            case "Cargar":
                cargarProvincia(request, response);
                break;
            case "Actualizar":
                actualizarProvincia(request, response);
                break;
            
        }
    }
    public void listarProvincia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<claseprovincia> provincia;
            modeloprovincia mP = new modeloprovincia();
            provincia = mP.obtenerProvincia();
            request.setAttribute("provincia", provincia);
            listarDepartamentos(request);
            request.getRequestDispatcher("vistas/listarprovincia.jsp").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }

    }
    
    public void registrarProvincia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        try 
        {
            int id_departamento = Integer.parseInt(request.getParameter("id_departamento"));
            String nombre = request.getParameter("txtnombre");
            claseprovincia nuevaprovincia = new claseprovincia(id_departamento, nombre);
            modeloprovincia mP = new modeloprovincia();
            mP.registrarProvincia(nuevaprovincia);
            response.sendRedirect("controladorprovincia");
            
        } catch (Exception e) {
            request.setAttribute("error", e);
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }
    
    public void cargarProvincia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        try 
        {
            int id = Integer.parseInt(request.getParameter("id_provin"));
            modeloprovincia mP = new modeloprovincia();
            claseprovincia provinciaCargada = mP.cargarProvincia(id);
            request.setAttribute("provinciaCargada", provinciaCargada);
            listarDepartamentos(request);
            request.getRequestDispatcher("vistas/actualizarprovincia.jsp").forward(request, response);
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
    
    public void actualizarProvincia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        try 
        {
            int id_provincia = Integer.parseInt(request.getParameter("id_provin"));
            int id_departamento = Integer.parseInt(request.getParameter("id_departamento"));
            String nombre = request.getParameter("txtnombre");
            claseprovincia provinciaActualizada = new claseprovincia(id_provincia, id_departamento, nombre);
            modeloprovincia mP = new modeloprovincia();
            mP.actualizarProvincia(provinciaActualizada);
            response.sendRedirect("controladorprovincia");
        }
        catch (Exception e) 
        {
            request.setAttribute("error", e);
            request.getRequestDispatcher("vistas/error.jsp").forward(request, response);
        }
    }
    
    public void listarDepartamentos(HttpServletRequest request) throws Exception {
        try 
        {
            List<clasedepartamento> departamentos;
            modelodepartamento md = new modelodepartamento();
            departamentos = md.obtenerDepartamentos();
            request.setAttribute("departamentos", departamentos);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }

}
