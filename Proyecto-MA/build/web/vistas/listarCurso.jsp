<%@page import="java.util.List"%>
<%@page import="modelo.claseCurso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Listado de Curso</title>
    </head>
    <%
        List<claseCurso> cursos = (List<claseCurso>)request.getAttribute("cursos");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Cursos</Strong></div>
                <div class="panel-body">    
                    
                    <form action="controladorCurso">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Nombre:</label>
                                <input type="text" name="txtNombre" placeholder="Ingrese Nombre" required class="form-control"/>
                                <label>Horas:</label>
                                <input type="number" name="txtHora" placeholder="Ingrese Hora" required class="form-control" />
                                <label>Creditos: </label>
                                <input type="number" name="txtCreditos" placeholder="Ingrese Creditos"  required class="form-control" />
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/cur.jpg" width="800"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                            <input type="reset" value="Nuevo" class="btn btn-success" />
                        </div> 
                    </form>
                    
                </div>
                <br>
                <!----------------------------Listado de Cursos -------------------------->
                
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Horas</th>
                            <th scope="col">Creditos</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (claseCurso cursoTemporal : cursos) {
                        %>
                        <tr class="resultados">
                            <td scope="col"><%=cursoTemporal.getNombre()%></td>
                            <td scope="col"><%=cursoTemporal.getNro_hora()%></td>
                            <td scope="col"><%=cursoTemporal.getNro_creditos()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorCurso?accion=Cargar&id_curso=<%=cursoTemporal.getId_curso()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorCurso?accion=Eliminar&id_curso=<%=cursoTemporal.getId_curso()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
        
    </body>
</html>