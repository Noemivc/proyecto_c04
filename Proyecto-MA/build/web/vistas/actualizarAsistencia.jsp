<%-- 
    Document   : actualizarAsistencia
    Created on : 15-ene-2019, 22:18:51
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseAsistencia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        claseAsistencia asistenciaCargado = (claseAsistencia)request.getAttribute("asistenciaCargado");
    %>
    <body>
        
        <div class="container">
            <%@include file="../menu.html" %>
            <form action="controladorAsistencia">
                <input type="hidden" name="id_asistencia" value="<%=asistenciaCargado.getId_asistencia()%>" />
                <br>
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Formulario de Actualización - Asistencia</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">         
                                <label>Dia:</label>
                                <input type="text" name="txtid_horario" class="form-control" required value="<%=asistenciaCargado.getId_horario()%>" />
                                <label>Estudiante:</label>
                                <input type="text" name="txtid_estudiante"  class="form-control" required  value="<%=asistenciaCargado.getId_estudiante()%>"/>
                                <label>Fecha:</label>
                                <input type="text" name="txtfecha" class="form-control" required value="<%=asistenciaCargado.getFecha()%>" />
                                <label>Estado:</label>
                                <input type="text" name="txttipo" placeholder="Ingrese estado" class="form-control" required value="<%=asistenciaCargado.getTipo()%>" />
                            </div>  
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <a href="#" class="thumbnail">
                                        <img src="img/car.jpg" width="250"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning"/>
                            <input type="reset" value="Deshacer" class="btn btn-success"/>
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorAsistencia'" />
                        </div>        
                    </div>
                </div>
            </form>
        </div>
        
    </body>
</html>
