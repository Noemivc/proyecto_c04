<%-- 
    Document   : registrarCarrera
    Created on : 06-dic-2018, 21:31:42
    Author     : DAMARIS VILLALOBOS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/estilos.ee.css" rel="stylesheet" type="text/css"/>
        <title>Carrera</title>
    </head>
    <body>
        <div class="container">
        <form action="controladorCarrera">
            <div class="row">
                <div class="col-lg-6">
                        Nombre:
                        <textarea name="txtnombre" placeholder="Ingrese Nombre de la Carrera" required class="form-control"></textarea>
                </div>  
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <a href="#" class="thumbnail">
                            <img src="img/car.jpg" width="250"/>
                        </a>
                    </div>
                </div>
            </div>
            <br>
            <div class="text-justify">
                <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                <input type="reset" value="Nuevo" class="btn btn-success" />
            </div>
        </form>
        </div>
    </body>
</html>
