
<%@page import="java.util.List"%>
<%@page import="modelo.clasedepartamento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        List<clasedepartamento> departamentos = (List<clasedepartamento>)request.getAttribute("departamentos");
    %>
    <body>
        <div class="container">
            <form action="controladorprovincia">
                <div class="row">
                    <div class="col-lg-4">
                        <label>Departamento:</label>
                        <select name="id_departamento" class="form-control">
                                <%
                                    for (clasedepartamento departamentoTemporal : departamentos) 
                                    {                                              
                                %>
                                            <option value="<%=departamentoTemporal.getIddep()%>" ><%=departamentoTemporal.getNombre()%></option>
                                <%        
                                    }
                                %>
                        </select>
                       
                        <label>Nombre:</label>
                        <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required class="form-control" />
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="img/dpd.png" width="200"/>
                            </a>
                        </div>
                    </div>
                </div>    
                <br>
                <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
                </div>                   
            </form>
        </div>
    </body>
</html>
