<%-- 
    Document   : listarDistrito
    Created on : 18-dic-2018, 21:01:39
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.modeloprovincia"%>
<%@page import="modelo.claseDistrito"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
     <%
        List<claseDistrito> distrito = (List<claseDistrito>)request.getAttribute("distrito");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Distritos</Strong></div>
                <div class="panel-body">
                    <%@include file="registrardistrito.jsp" %>
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th>ID</th>
                            <th>Provincia</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (claseDistrito distritoTemporal : distrito) {
                        %>
                        <tr>
                            <td><%=distritoTemporal.getId_distri()%></td>
                            <td>
                                <%
                                    modeloprovincia md = new modeloprovincia();
                                    String nombre = md.obtenerNombre(distritoTemporal.getId_provincia());
                                %>
                                <%=nombre%>
                            </td>
                            <td><%=distritoTemporal.getNombre()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorDistrito?accion=Cargar&id_distri=<%=distritoTemporal.getId_distri()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorDistrito?accion=Eliminar&id_distri=<%=distritoTemporal.getId_distri()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
        
    </body>
</html>
