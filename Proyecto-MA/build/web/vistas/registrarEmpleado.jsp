<%-- 
    Document   : registrarEmpleado
    Created on : 05-dic-2018, 21:27:55
    Author     : DAMARIS VILLALOBOS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
        <form action="controladorEmpleado">
            <div class="row">
                <div class="col-lg-4">
                    <label>DNI:</label>
                    <input type="text" name="txtdni" placeholder="Ingrese DNI" required class="form-control" />
                    <label>Nombre:</label>
                    <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required class="form-control" />
                    <label>Cargo:</label>
                    <input type="text" name="txtcargo" placeholder="Ingrese Cargo" required class="form-control" />
                </div>
                <div class="col-lg-4">
                    <label>Usuario:</label>
                    <input type="text" name="txtusu" placeholder="Ingrese usuario" required class="form-control" />
                    <label>Clave:</label>
                    <input type="text" name="txtclave" placeholder="Ingrese Clave" required class="form-control" />                      
                    <label>Estado:</label>
                    <input type="text" name="txtestado" placeholder="Ingrese Estado"class="form-control" />                   
                </div> 
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <a href="#" class="thumbnail">
                            <img src="img/emp.jpg" width="200"/>
                        </a>
                    </div>
                </div>
            </div> 
            <br>
            <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
            </div>
        </form>
        </div>
    </body>
</html>
