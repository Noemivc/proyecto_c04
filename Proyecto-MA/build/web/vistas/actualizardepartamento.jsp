<%-- 
    Document   : actualizardepartamento
    Created on : 07/12/2018, 05:26:24 PM
    Author     : Lenovo
--%>

<%@page import="modelo.clasedepartamento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
     <%
        clasedepartamento departamentoCargado = (clasedepartamento)request.getAttribute("departamentoCargado");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorDistrito">
                <input type="hidden" name="id_depar" value="<%=departamentoCargado.getIddep() %>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Distrito</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Nombre:</label>
                                <input type="text" name="txtNombre" placeholder="Ingrese Nombre" required value="<%=departamentoCargado.getNombre()%>" />
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/dpd.png" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladordepartamento'" />
                        </div>
                    </div>

                </div>
            </form>
        </div> 
    </body>
</html>
