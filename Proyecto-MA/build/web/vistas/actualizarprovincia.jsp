<%-- 
    Document   : actualizarprovincia
    Created on : 07/12/2018, 05:27:06 PM
    Author     : Lenovo
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.clasedepartamento"%>
<%@page import="modelo.claseprovincia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%
        claseprovincia provinciaCargada = (claseprovincia)request.getAttribute("provinciaCargada");
        List<clasedepartamento> departamentos = (List<clasedepartamento>)request.getAttribute("departamentos");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorprovincia">
                <input type="hidden" name="id_provin" value="<%=provinciaCargada.getId_provin()%>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Empleados</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Id Departamento:</label>
                                <select name="id_departamento" class="form-control">
                                <%
                                    for (clasedepartamento departamentoTemporal : departamentos) 
                                    {
                                        if (departamentoTemporal.getIddep()==provinciaCargada.getId_departamento()) 
                                        {
                                                
                                %>
                                            <option selected value="<%=departamentoTemporal.getIddep()%>" ><%=departamentoTemporal.getNombre()%></option>
                                <%  
                                        }else
                                        {
                                %>
                                            <option value="<%=departamentoTemporal.getIddep()%>" ><%=departamentoTemporal.getNombre()%></option>
                                <%        
                                        }
  
                                    }
                                %>
                                </select>
                               
                                <label>Nombre:</label>
                                <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required value="<%= provinciaCargada.getNombre()%>" class="form-control" />
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/dpd.png" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorprovincia'" />
                        </div>
                    </div>

                </div>                    
            </form>
        </div>
    </body>
</html>
