<%-- 
    Document   : aula
    Created on : 07/12/2018, 10:48:27 AM
    Author     : Administrador
--%>

<%@page import="modelo.claseAula"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aula</title>
    </head>
    <body>
    <%
        List<claseAula> aulas = (List<claseAula>)request.getAttribute("aulas");
    %>
    </body>
    <div class="container">
        <%@include file="../menu.html" %>
        <div class="panel panel-primary">
            <div class="panel-heading"><Strong>Aulas</Strong></div>
            <div class="panel-body">
                <%@include file="/vistas/registrarAula.jsp" %>
            </div>
            <br>
            <table class="table table-sm table-bordered  table-hover">
                <thead class="alert alert-info">
                    <tr>
                        <th>Tipo de Aula</th>
                        <th>Número</th>
                        <th>Piso</th>
                        <th><div class="text-center">Acciones</div></th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (claseAula aulaTemporal : aulas) {
                    %>
                    <tr>
                        <td><%
                            if (aulaTemporal.getTipoAula().equals("Laboratorio")) {
                            %>Laboratorio<%
                                } else {
                            %>Aula de Fotografia<%
                                    }
                            %></td>
                        <td><%=aulaTemporal.getNumero()%></td>
                        <td><%=aulaTemporal.getPiso()%></td>
                        <td>
                            <div class="text-center">
                                <a href="controladorAula?accion=Cargar&id_aula=<%=aulaTemporal.getId_aula()%>">
                                    <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                </a>
                                <a href="controladorAula?accion=Eliminar&id_aula=<%=aulaTemporal.getId_aula()%>">
                                    <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <br>
        </div>
</html>
