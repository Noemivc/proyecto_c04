<%-- 
    Document   : empleado
    Created on : 06-dic-2018, 22:04:09
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseEmpleado"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        List<claseEmpleado> empleados = (List<claseEmpleado>)request.getAttribute("empleados");
    %>
    <body>
                              
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Empleados</Strong></div>
                <div class="panel-body">
                    
                    <form action="controladorEmpleado">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>DNI:</label>
                                <input type="text" name="txtdni" placeholder="Ingrese DNI" required class="form-control" />
                                <label>Nombre:</label>
                                <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required class="form-control" />
                                <label>Cargo:</label>
                                <input type="text" name="txtcargo" placeholder="Ingrese Cargo" required class="form-control" />
                            </div>
                            <div class="col-lg-4">
                                <label>Usuario:</label>
                                <input type="text" name="txtusu" placeholder="Ingrese usuario" required class="form-control" />
                                <label>Clave:</label>
                                <input type="password" name="txtclave" placeholder="Ingrese Clave" required class="form-control" />                      
                                <label>Estado:</label>
                                <input type="text" name="txtestado" placeholder="Ingrese Estado"class="form-control" />                   
                            </div> 
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/emp.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                            <input type="reset" value="Nuevo" class="btn btn-success" />
                        </div>
                    </form>
                    
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th>DNI</th>
                            <th>NOMBRE</th>
                            <th>CARGO</th>
                            <th>USUARIO</th>
                            <th>ESTADO</th>
                            <th><div class="text-center">ACCIONES</div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (claseEmpleado empleadoTemporal : empleados) {
                        %>
                        <tr>
                            <td><%=empleadoTemporal.getDni()%></td>
                            <td><%=empleadoTemporal.getNombre()%></td>
                            <td><%=empleadoTemporal.getCargo()%></td> 
                            <td><%=empleadoTemporal.getUsuario()%></td>
                            <td><%=empleadoTemporal.getEstado()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorEmpleado?accion=Cargar&id_emple=<%=empleadoTemporal.getId_Empleado()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorEmpleado?accion=Eliminar&id_emple=<%=empleadoTemporal.getId_Empleado()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>

    </body>
</html>
