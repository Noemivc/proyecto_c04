<%-- 
    Document   : registrarMatricula
    Created on : 19/12/2018, 05:52:06 PM
    Author     : Lenovo
--%>

<%@page import="modelo.claseEmpleado"%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseEstudiante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Matricula</title>
    </head>
    <%
        List<claseEstudiante> estudiante = (List<claseEstudiante>)request.getAttribute("estudiante");
        List<claseEmpleado> empleado = (List<claseEmpleado>)request.getAttribute("empleado");
    %>
    <body>
      <div class="container">
            <form action="controladorMatricula">

                <div class="row">
                    <div class="col-lg-4">
                         <label>Id Estudiantes:</label>
                        <select name="id_estudiantes" class="form-control">
                                <%
                                    for (claseEstudiante estudianteTemporal : estudiante) 
                                    {                                              
                                %>
                                            <option value="<%=estudianteTemporal.getId_estudiante()%>" ><%=estudianteTemporal.getNombre()%></option>
                                <%        
                                    }
                                %>
                        </select>
                        
                        <label>Fecha:</label>
                        <input type="date" name="txtFecha" required class="form-control" >
                        <label>Ciclo:</label>
                         <input type="text" name="txtCiclo" placeholder="ingrese ciclo" required class="form-control" >
                        <label>Condiciones</label>
                        <select id="txtCondiciones" name="txtCondiciones" class="form-control">
                            <option selected>--Seleccione el tipo de Condición--</option>
                            <option value="B">Becado</option>
                            <option value="NB">No Becado</option>
                        </select>
                        <label>Empleado</label>
                        <select name="id_Empleado" class="form-control">
                                <%
                                    for (claseEmpleado empleadoTemporal : empleado) 
                                    {                                              
                                %>
                                            <option value="<%=empleadoTemporal.getId_Empleado()%>" ><%=empleadoTemporal.getNombre()%></option>
                                <%        
                                    }
                                %>
                        </select>
                     </div> 
                    <div class="row">
                        <%-- <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="img/emp.jpg" width="200"/>
                            </a>
                        </div>
                        --%>
                    </div>
                </div> 
                <br>
                <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
                </div>
            </form>
        </div>
        
    </body>
</html>
