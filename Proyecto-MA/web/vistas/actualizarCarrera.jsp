<%-- 
    Document   : actualizarCarrera
    Created on : 06-dic-2018, 21:40:55
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseCarrera"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrera</title>
    </head>
    <%
        claseCarrera carreraCargada = (claseCarrera)request.getAttribute("carreraCargada");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
        <form action="controladorCarrera">
            <input type="hidden" name="id_carre" value="<%=carreraCargada.getId_carrera()%>" />
            
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Carreras</Strong></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            Nombre:
                            <textarea name="txtnombre" id="txtnombre" placeholder="Ingrese Nombre de la Carrera" required class="form-control"><%=carreraCargada.getNombre()%></textarea>
                        </div>  
                        <div class="row">
                            <div class="col-xs-6 col-md-4">
                                <a href="#" class="thumbnail">
                                    <img src="img/car.jpg" width="250"/>
                                </a>
                            </div>
                        </div>
                    </div> 
                    <br>
                    <div class="text-justify">
                        <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                        <input type="reset" value="Deshacer" class="btn btn-success" />
                        <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorCarrera'" />
                    </div>        
                </div>
            </div>
        </form>
        </div>
    </body>
</html>
