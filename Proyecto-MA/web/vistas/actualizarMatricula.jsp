<%-- 
    Document   : actualizarMatricula
    Created on : 19/12/2018, 06:36:04 PM
    Author     : Lenovo
--%>

<%@page import="modelo.claseEstudiante"%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseEmpleado"%>
<%@page import="modelo.clasematricula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
      <%
        clasematricula matriculaCargada = (clasematricula)request.getAttribute("matriculaCargada");
        List<claseEmpleado> empleado = (List<claseEmpleado>)request.getAttribute("empleado");
        List<claseEstudiante> estudiante = (List<claseEstudiante>)request.getAttribute("estudiante");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>           
            <form action="controladorMatricula">
                <input type="hidden" name="id_matricula" value="<%=matriculaCargada.getId_matricula()%>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Matricula</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Estudiantes:</label>        
                                <select name="id_estudiantes" class="form-control">
                                <%
                                    for (claseEstudiante estudianteTemporal : estudiante) 
                                    {
                                        if (estudianteTemporal.getId_estudiante()==matriculaCargada.getId_estudiantes()) 
                                        {
                                                
                                %>
                                            <option selected value="<%=estudianteTemporal.getId_estudiante()%>" ><%=estudianteTemporal.getNombre()%></option>
                                <%  
                                        }else
                                        {
                                %>
                                            <option value="<%=estudianteTemporal.getId_estudiante()%>" ><%=estudianteTemporal.getNombre()%></option>
                                <%        
                                        }
  
                                    }
                                %>
                                </select>
                                
                                <label>Fecha:</label>
                                <input type="date" name="txtFecha"  required value="<%=matriculaCargada.getFecha()%>" class="form-control"/>
                                <label>Ciclo:</label>
                                <input type="number" name="txtCiclo"  required value="<%=matriculaCargada.getCiclo()%>" class="form-control"/>
                            </div>
                            <div class="col-lg-4">
                                <label>Condiciones:</label>
                                <select id="aula" name="txtCondiciones" class="form-control">
                                    <%
                                        if (matriculaCargada.getCondiciones().equals("B")) {
                                    %><option selected>B</option>
                                    <option >NB</option><%
                                    } else {
                                    %><option >B</option>
                                    <option selected>NB</option><%
                                        }
                                    %>
                                </select>
                                <label>Empleado:</label>
                                <select name="id_Empleado" class="form-control">
                                <%
                                    for (claseEmpleado empleadoTemporal : empleado) 
                                    {
                                        if (empleadoTemporal.getId_Empleado()==matriculaCargada.getId_Empleado()) 
                                        {
                                                
                                %>
                                            <option selected value="<%=empleadoTemporal.getId_Empleado()%>" ><%=empleadoTemporal.getNombre()%></option>
                                <%  
                                        }else
                                        {
                                %>
                                            <option value="<%=empleadoTemporal.getId_Empleado()%>" ><%=empleadoTemporal.getNombre()%></option>
                                <%        
                                        }
  
                                    }
                                %>
                                </select>
                            
                            </div> 
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/emp.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorMatricula'" />
                        </div>
                    </div>
                </div>
            </form>       
        </div>
    </body>
</html>
