<%-- 
    Document   : listarHorario
    Created on : 04-ene-2019, 18:55:44
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.modeloAula"%>
<%@page import="modelo.modeloCurso"%>
<%@page import="modelo.modeloCarrera"%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseHorario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        List<claseHorario> horario = (List<claseHorario>)request.getAttribute("horario");
    %>
    <body>
        
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Horario</Strong></div>
                <div class="panel-body">
                    
                    <form action="controladorHorario">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>CARRERA:</label>
                                <input type="text" name="txtid_carrera"  required class="form-control" />
                                <label>ESTUDIANTE:</label>
                                <input type="text" name="txtid_estudiante"  required class="form-control" />
                                <label>CURSO:</label>
                                <input type="text" name="txtid_cursos"  required class="form-control" />
                                <label>DOCENTE:</label>
                                <input type="text" name="txtid_docente" required class="form-control" />
                            </div>
                            <div class="col-lg-4">
                                <label>AULA:</label>
                                <input type="text" name="txtid_aula" placeholder="Ingrese Aula" required class="form-control" />
                                <label>DIA:</label>
                                <input type="text" name="txtdia" placeholder="Ingrese Dia" required class="form-control" />                      
                                <label>HORA DE INICIO:</label>
                                <input type="text" name="txthorainicio" placeholder="Ingrese hra de inicio"class="form-control" />  
                                <label>HORA DE FIN:</label>
                                <input type="text" name="txthorafin" placeholder="Ingrese hra de fin"class="form-control" />
                            </div> 
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/emp.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                            <input type="reset" value="Nuevo" class="btn btn-success" />
                        </div>
                    </form>
                    
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th>CARRERA</th>
                            <th>CURSO</th>
                            <th>DOCENTE</th>
                            <th>AULA</th>
                            <th>DIA</th>
                            <th>HORA DE INICIO</th>
                            <th>HORA DE FIN</th>
                            <th><div class="text-center">ACCIONES</div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (claseHorario horarioTemporal : horario) {
                        %>
                        <tr>
                            <td>
                                <%
                                    modeloCarrera me = new modeloCarrera();
                                    String nombreC = me.obtenerNombre(horarioTemporal.getId_carrera());
                                %>
                                <%=nombreC%>
                            </td>
                            <td>
                                <%
                                    modeloCurso mc = new modeloCurso();
                                    String nombreCC = mc.obtenerNombre(horarioTemporal.getId_cursos());
                                %>
                                <%=nombreCC%>
                            </td>
                            <td><%=horarioTemporal.getId_docente()%></td> 
                            <td>
                                <%
                                    modeloAula ma = new modeloAula();
                                    String nombreA = ma.obtenerNombre(horarioTemporal.getId_aula());
                                %>
                                <%=nombreA%>
                            </td>
                            <td><%=horarioTemporal.getDia()%></td>
                            <td><%=horarioTemporal.getHorainicio()%></td>
                            <td><%=horarioTemporal.getHorafin()%></td>
                            <td>
                                <div class="text-center">
                                    <a data-toggle="tooltip" data-placement="top" title="Actualizar" href="controladorHorario?accion=Cargar&id_horario=<%=horarioTemporal.getId_horario()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a data-toggle="tooltip" data-placement="top" title="Eliminar" href="controladorHorario?accion=Eliminar&id_horario=<%=horarioTemporal.getId_horario()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
        
    </body>
</html>
