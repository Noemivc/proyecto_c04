
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <form action="controladorAula">

                <div class="row">
                    <div class="col-lg-4">
                        <label>Tipo de Aula:</label>
                        <select id="txtaula" name="txtaula" class="form-control">
                            <option selected>--Seleccione tipo de Aula--</option>
                            <option>Laboratorio</option>
                            <option>Aula de Fotografia</option>
                        </select>
                        <label>Número:</label>
                        <input type="number" name="txtnumero" placeholder="ingrese N° aula" required class="form-control" >
                        <label>Piso:</label>
                        <input type="number" name="txtpiso" placeholder="ingrese N° piso" required class="form-control" >
                    </div> 
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="img/emp.jpg" width="200"/>
                            </a>
                        </div>
                    </div>
                </div> 
                <br>
                <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
                </div>
            </form>
        </div>
    </body>
</html>
