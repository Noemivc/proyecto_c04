

<%@page import="modelo.modeloDistrito"%>
<%@page import="modelo.claseEstudiante"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Listado de Productos</title>
    </head>
    <%
        List<claseEstudiante> estudiante = (List<claseEstudiante>)request.getAttribute("estudiante");
        
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Estudiantes</Strong></div>
                <div class="panel-body">
                    <%@include file="registrarEstudiante.jsp" %>
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <td>Dni</td>
                            <td>Nombre</td>
                            <td>Apellidos</td>
                            <td>Genero</td>
                            <td>Celular</td>
                            <td>Correo</td>
                            <td>Fecha De Nacimiento</td>
                            <td>Lugar De Nacimiento</td>
                            <td>Distrito</td>
                            <td>Direccion</td>
                            <td>Estado</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>
                    <tbody> 
                        <%
                            for (claseEstudiante estudianteTemporal : estudiante) {
                        %>      
                        <tr>
                            <td><%=estudianteTemporal.getDni()%></td>
                            <td><%=estudianteTemporal.getNombre()%></td>
                            <td><%=estudianteTemporal.getApellidos()%></td>
                            <td><%=estudianteTemporal.getGenero()%></td>
                            <td><%=estudianteTemporal.getCelular()%></td>
                            <td><%=estudianteTemporal.getCorreo()%></td>
                            <td><%=estudianteTemporal.getFecha_nac()%></td>
                            <td><%=estudianteTemporal.getLugar_nac()%></td>
                            <td>
                                <%
                                    modeloDistrito md = new modeloDistrito();
                                    String nombre = md.obtenerNombre(estudianteTemporal.getId_distrito());
                                %>
                                <%=nombre%>
                            </td>
                            <td><%=estudianteTemporal.getDireccion()%></td>
                            <td><%=estudianteTemporal.getEstado()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorEstudiante?accion=Cargar&id_estudiante=<%=estudianteTemporal.getId_estudiante()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorEstudiante?accion=Eliminar&id_estudiante=<%=estudianteTemporal.getId_estudiante()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </tbody> 
                    <%
                        }
                    %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
    </body>
</html>
