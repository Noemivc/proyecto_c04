<%-- 
    Document   : actualizarHorario
    Created on : 04-ene-2019, 19:40:50
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseHorario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%
        claseHorario horarioCargado = (claseHorario)request.getAttribute("horarioCargado");
    %>
    <body>
        
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorHorario">
                <input type="hidden" name="id_horario" value="<%=horarioCargado.getId_horario()%>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Horario</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Carrera:</label>
                                <input type="text" name="txtid_carrera" value="<%=horarioCargado.getId_carrera()%>" placeholder="Ingrese Carrera" required class="form-control" />
                                <label>Curso:</label>
                                <input type="text" name="txtid_cursos" value="<%=horarioCargado.getId_cursos()%>" placeholder="Ingrese Curso" required class="form-control" />
                                <label>Docente:</label>
                                <input type="text" name="txtid_docente" value="<%=horarioCargado.getId_docente()%>" placeholder="Ingrese Docente" required class="form-control" />
                            </div>
                            <div class="col-lg-4">
                                <label>Aula:</label>
                                <input type="text" name="txtid_aula" value="<%=horarioCargado.getId_aula()%>" placeholder="Ingrese Aula" required class="form-control" />
                                <label>Dia:</label>
                                <input type="text" name="txtdia" value="<%=horarioCargado.getDia()%>" placeholder="Ingrese Dia" required class="form-control" />                      
                                <label>Hora de Inicio:</label>
                                <input type="text" name="txthorainicio" value="<%=horarioCargado.getHorainicio()%>" placeholder="Ingrese Hra de Inicio"class="form-control" />   
                                <label>Hora de Fin:</label>
                                <input type="text" name="txthorafin" value="<%=horarioCargado.getHorafin()%>" placeholder="Ingrese Hra de Fin"class="form-control" />
                            </div> 
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/emp.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorHorario'" />
                        </div>
                    </div>

                </div>
            </form>
        </div>
        
    </body>
</html>
