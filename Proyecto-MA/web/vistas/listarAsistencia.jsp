<%-- 
    Document   : listarAsistencia
    Created on : 15-ene-2019, 22:06:05
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.claseAsistencia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        List<claseAsistencia> asistencia = (List<claseAsistencia>)request.getAttribute("asistencia");
    %>
    <body>
        
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Asistencia</Strong></div>
                <div class="panel-body">    
                    
                    <form action="controladorAsistencia">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Horario:</label>
                                <input type="text" name="txtid_horario"  required class="form-control"/>
                                <label>Estudiante:</label>
                                <input type="text" name="txtid_estudiante"  required class="form-control" />
                                <label>Fecha: </label>
                                <input type="date" name="txtfecha"  required class="form-control" />
                                <label>Tipo: </label>
                                <select name="txttipo" class="form-control">
                                    <option id="1">Asistio</option>
                                    <option id="2">No Asistio</option>
                                    <option id="3">Tardanza</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/cur.jpg" width="800"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                            <input type="reset" value="Nuevo" class="btn btn-success" />
                        </div> 
                    </form>
                    
                </div>
                <br>
                <!----------------------------Listado de Asistencia -------------------------->
                
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th scope="col">Horario</th>
                            <th scope="col">Estudiante</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (claseAsistencia asistenciaTemporal : asistencia) {
                        %>
                        <tr class="resultados">
                            <td scope="col"><%=asistenciaTemporal.getId_horario()%></td>
                            <td scope="col"><%=asistenciaTemporal.getId_estudiante()%></td>
                            <td scope="col"><%=asistenciaTemporal.getFecha()%></td>
                            <td scope="col"><%=asistenciaTemporal.getTipo()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorAsistencia?accion=Cargar&id_asistencia=<%=asistenciaTemporal.getId_asistencia()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorAsistencia?accion=Eliminar&id_asistencia=<%=asistenciaTemporal.getId_asistencia()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
        
    </body>
</html>
