<%-- 
    Document   : listarprovincia
    Created on : 07/12/2018, 05:25:33 PM
    Author     : Lenovo
--%>

<%@page import="modelo.modelodepartamento"%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseprovincia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
     <%
        List<claseprovincia> provincia = (List<claseprovincia>)request.getAttribute("provincia");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Provincias</Strong></div>
                <div class="panel-body">
                    <%@include file="/vistas/registrarProvincia.jsp" %>
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Departamento</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <%
                            for (claseprovincia provinciaTemporal : provincia) {
                        %>
                        <tr>
                            <td><%=provinciaTemporal.getId_provin()%> </td>
                            <td>
                                <%
                                    modelodepartamento md = new modelodepartamento();
                                    String nombre = md.obtenerNombre(provinciaTemporal.getId_departamento());
                                %>
                                <%=nombre%>
                            </td>
                            <td><%=provinciaTemporal.getNombre()%></td>
                            <td>
                                <div class="text-center">
                                    <a href="controladorprovincia?accion=Cargar&id_provin=<%=provinciaTemporal.getId_provin()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
    </body>
</html>
