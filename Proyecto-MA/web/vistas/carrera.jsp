<%-- 
    Document   : carrera
    Created on : 06-dic-2018, 22:01:13
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseCarrera"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrera</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <%
        List<claseCarrera> carreras = (List<claseCarrera>)request.getAttribute("carreras");
    %>
    <body>
         <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Carreras</Strong></div>
            <div class="panel-body">
            <%@include file="/vistas/registrarCarrera.jsp" %>
            </div>
            <br>
        <table class="table table-sm table-bordered  table-hover">
            <thead class="alert alert-info">
            <tr>
                <th>CODIGO</th>
                <th>NOMBRE</th>
                <th><div class="text-center">ACCIONES</div></th>
            </tr>
            </thead>
            <tbody>
            <%
                for (claseCarrera carreraTemporal : carreras) 
                {
                    %>
                    <tr>
                        <td><%=carreraTemporal.getId_carrera()%></td>
                        <td><%=carreraTemporal.getNombre()%></td>
                        <td>
                            <div class="text-center">
                            <a href="controladorCarrera?accion=Cargar&id_carre=<%=carreraTemporal.getId_carrera()%>">
                                <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                            </a>
                            <a href="controladorCarrera?accion=Eliminar&id_carre=<%=carreraTemporal.getId_carrera()%>">
                                <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                            </a>
                            </div>
                        </td>
                    </tr>
                    <%
                }
            %>
            </tbody>
        </table>
            <br>
        </div>
    </div>
    </body>
</html>
