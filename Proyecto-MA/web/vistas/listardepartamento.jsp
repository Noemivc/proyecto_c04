<%-- 
    Document   : listardepartamento
    Created on : 07/12/2018, 05:24:58 PM
    Author     : Lenovo
--%>
<%@page import="java.util.List"%>
<%@page import="modelo.clasedepartamento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
     <%
        List<clasedepartamento> departamento = (List<clasedepartamento>)request.getAttribute("departamento");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Departamentos</Strong></div>
                <div class="panel-body">
                    <%@include file="registrarDepartamento.jsp" %>
                </div>
                <br>  


                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (clasedepartamento departamentoTemporal : departamento) {
                        %>
                        <tr>
                            <td class="text-center"><%=departamentoTemporal.getNombre()%></td>
                            <td class="text-center">
                                <div class="text-center">
                                <a href="controladordepartamento?accion=Cargar&id_depar=<%=departamentoTemporal.getIddep()%>">
                                    <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                </a>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div>
    </body>
</html>
