
<%@page import="modelo.claseAula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aula</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <%
        claseAula aulaCargada = (claseAula)request.getAttribute("aulaCargada");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <form action="controladorAula">
                <input type="hidden" name="id_aula" value="<%=aulaCargada.getId_aula()%>" />
                <br>
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Formulario de Actualización - Aulas</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Tipo de Aula:</label>
                                <select id="aula" name="txtaula" class="form-control">
                                    <%
                                        if (aulaCargada.getTipoAula().equals("Laboratorio")) {
                                    %><option selected>Laboratorio</option>
                                    <option >Aula de Fotografia</option><%
                                    } else {
                                    %><option >Laboratorio</option>
                                    <option selected>Aula de Fotografia</option><%
                                        }
                                    %>
                                </select>
                                <label>Número:</label>
                                <input type="number" name="txtnumero" placeholder="Ingrese N° aula" class="form-control" required value="<%=aulaCargada.getNumero()%>" />
                                <label>Piso:</label>
                                <input type="number" name="txtpiso" placeholder="Ingrese N° piso" class="form-control" required value="<%=aulaCargada.getPiso()%>" />
                            </div>  
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <a href="#" class="thumbnail">
                                        <img src="img/car.jpg" width="250"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning"/>
                            <input type="reset" value="Deshacer" class="btn btn-success"/>
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorAula'" />
                        </div>        
                    </div>
                </div>

            </form>
        </div>
    </body>
</html>
