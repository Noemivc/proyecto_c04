<%-- 
    Document   : actualizarEmpleado
    Created on : 05-dic-2018, 22:22:35
    Author     : DAMARIS VILLALOBOS
--%>

<%@page import="modelo.claseEmpleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Empleado</title>
    </head>
    <%
        claseEmpleado empleadoCargado = (claseEmpleado)request.getAttribute("empleadoCargado");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorEmpleado">
                <input type="hidden" name="id_emple" value="<%=empleadoCargado.getId_Empleado()%>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Empleados</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>DNI:</label>
                                <input type="text" name="txtdni" value="<%=empleadoCargado.getDni()%>" placeholder="Ingrese DNI" required class="form-control" />
                                <label>Nombre:</label>
                                <input type="text" name="txtnombre" value="<%=empleadoCargado.getNombre()%>" placeholder="Ingrese Nombre" required class="form-control" />
                                <label>Cargo:</label>
                                <input type="text" name="txtcargo" value="<%=empleadoCargado.getCargo()%>" placeholder="Ingrese Cargo" required class="form-control" />
                            </div>
                            <div class="col-lg-4">
                                <label>Usuario:</label>
                                <input type="text" name="txtusu" value="<%=empleadoCargado.getUsuario()%>" placeholder="Ingrese usuario" required class="form-control" />
                                <label>Clave:</label>
                                <input type="text" name="txtclave" value="<%=empleadoCargado.getClave()%>" placeholder="Ingrese Clave" required class="form-control" />                      
                                <label>Estado:</label>
                                <input type="text" name="txtestado" value="<%=empleadoCargado.getEstado()%>" placeholder="Ingrese Estado"class="form-control" />                   
                            </div> 
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/emp.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorEmpleado'" />
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </body>
</html>
