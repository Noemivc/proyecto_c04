<%-- 
    Document   : listarmatricula
    Created on : 19/12/2018, 04:57:13 PM
    Author     : Lenovo
--%>

<%@page import="modelo.modeloEstudiante"%>
<%@page import="modelo.modeloEmpleado"%>
<%@page import="java.util.List"%>
<%@page import="modelo.clasematricula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
      <%
        List<clasematricula> matricula = (List<clasematricula>)request.getAttribute("matricula");
    %>
    <body>
     <div class="container">
            <%@include file="../menu.html" %>
            <div class="panel panel-primary">
                <div class="panel-heading"><Strong>Matricula</Strong></div>
                <div class="panel-body">
                    <%@include file="registrarMatricula.jsp" %>
                </div>
                <br>
                <table class="table table-sm table-bordered  table-hover">
                    <thead class="alert alert-info">
                        <tr>
                            <td>Id Estudiantes</td>
                            <td>Fecha</td>
                            <td>Ciclo</td>
                            <td>Condiciones</td>
                            <td>Empleado</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>
                    <tbody> 
                        <%
                            for (clasematricula matriculaTemporal : matricula) {
                        %>      
                        <tr>
                            <td>
                                <%
                                    modeloEstudiante md = new modeloEstudiante();
                                    String nombree = md.obtenerNombre(matriculaTemporal.getId_estudiantes());
                                %>
                                <%=nombree%>
                            </td>
                            <td><%=matriculaTemporal.getFecha()%></td>
                            <td><%=matriculaTemporal.getCiclo()%></td>
                            <td><%=matriculaTemporal.getCondiciones()%></td>
                            <td>
                                <%
                                    modeloEmpleado me = new modeloEmpleado();
                                    String nombre = me.obtenerNombre(matriculaTemporal.getId_Empleado());
                                %>
                                <%=nombre%>
                            </td>
                         <td>
                                <div class="text-center">
                                    <a href="controladorMatricula?accion=Cargar&id_matricula=<%=matriculaTemporal.getId_matricula()%>">
                                        <span class="btn btn-primary glyphicon glyphicon-refresh"></span>
                                    </a>
                                    <a href="controladorMatricula?accion=Eliminar&id_matricula=<%=matriculaTemporal.getId_matricula()%>">
                                        <span class="btn btn-danger glyphicon glyphicon-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </tbody> 
                    <%
                        }
                    %>
                    </tbody>
                </table>
                <br>  
            </div>
        </div> 
        
    </body>
</html>
