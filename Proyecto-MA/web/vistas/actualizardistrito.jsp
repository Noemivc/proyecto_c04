<%-- 
    Document   : actualizardistrito
    Created on : 07/12/2018, 05:26:44 PM
    Author     : Lenovo
--%>

<%@page import="modelo.claseprovincia"%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseDistrito"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <%
        claseDistrito distritoCargado = (claseDistrito)request.getAttribute("distritoCargado");
        List<claseprovincia> provincias = (List<claseprovincia>)request.getAttribute("provincias");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorDistrito">
                <input type="hidden" name="id_distri" value="<%= distritoCargado.getId_distri()%>" />
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Distrito</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Provincia:</label>
                                <select name="id_provin" class="form-control">
                                <%
                                    for (claseprovincia provinciaTemporal : provincias) 
                                    {
                                        if (provinciaTemporal.getId_provin()==distritoCargado.getId_provincia()) 
                                        {                                               
                                %>
                                            <option selected value="<%=provinciaTemporal.getId_provin()%>" ><%=provinciaTemporal.getNombre()%></option>
                                <%  
                                        }else
                                        {
                                %>
                                            <option value="<%=provinciaTemporal.getId_provin()%>" ><%=provinciaTemporal.getNombre()%></option>
                                <%        
                                        } 
                                    }
                                %>
                                </select>
                                
                                <label>Nombre:</label>
                                <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required value="<%= distritoCargado.getNombre()%>" class="form-control" />
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img src="img/dpd.png" width="200"/>
                                    </a>
                                </div>
                            </div>
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer" class="btn btn-success" />
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorDistrito'" />
                        </div>
                    </div>

                </div>
            </form>
        </div>                
    </body>
</html>
