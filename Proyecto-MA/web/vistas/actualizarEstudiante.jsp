<%-- 
    Document   : actualizarEstudiante
    Created on : 18-dic-2018, 17:34:23
    Author     : LUIS
--%>
<%@page import="java.util.List"%>
<%@page import="modelo.claseDistrito"%>
<%@page import="modelo.claseEstudiante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Actualización de Productos</title>
    </head>
    <%
        claseEstudiante estudianteCargado = (claseEstudiante)request.getAttribute("estudianteCargado");
        List<claseDistrito> distritos = (List<claseDistrito>)request.getAttribute("distritos");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <br><br>
            <form action="controladorEstudiante">
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Empleados</Strong></div>
                    <div class="panel-body">
                        <input type="hidden" name="id_estudiante" value="<%=estudianteCargado.getId_estudiante()%>" />
                        <div class="row">
                            <div class="col-lg-4">
                                <label>Dni:</label>
                                <input type="number" name="txtDni" placeholder="Ingrese Dni" value="<%=estudianteCargado.getDni()%>" required class="form-control" />
                                <label>Nombre:</label>
                                <input type="text" name="txtNombre" placeholder="Ingrese Nombre" value="<%=estudianteCargado.getNombre()%>" required class="form-control" />
                                <label>Apellidos:</label>
                                <input type="text" name="txtApellidos" placeholder="Ingrese Apellidos" value="<%=estudianteCargado.getApellidos()%>" required class="form-control" />
                                <label>Genero:</label>
                                <input type="text" name="txtGenero" placeholder="Ingrese Genero:" value="<%=estudianteCargado.getGenero()%>" required class="form-control" />
                                <label>Celular:</label>
                                <input type="number" name="txtCelular" placeholder="Ingrese Celular" value="<%=estudianteCargado.getCelular()%>" required class="form-control" />
                            </div>   
                            <div class="col-lg-4">
                                <label>Correo:</label>
                                <input type="email" name="txtCorreo" placeholder="Ingrese Correo" value="<%=estudianteCargado.getCorreo()%>" required class="form-control" />
                                <label>Fecha De Nacimiento:</label>
                                <input type="date" name="txtFecha_nac" placeholder="Ingrese F.nacimiento" value="<%=estudianteCargado.getFecha_nac()%>" required class="form-control" />
                                <label>Lugar De Nacimiento:</label>
                                <input type="text" name="txtLugar_nac" placeholder="Ingrese L.nacimiento" value="<%=estudianteCargado.getLugar_nac()%>" required class="form-control" />
                                <label>Distrito:</label>
                                
                                <select name="id_distrito" class="form-control">
                                <%
                                    for (claseDistrito distritoTemporal : distritos) 
                                    {
                                        if (distritoTemporal.getId_distri()==estudianteCargado.getId_distrito()) 
                                        {
                                                
                                %>
                                            <option selected value="<%=distritoTemporal.getId_distri()%>" ><%=distritoTemporal.getNombre()%></option>
                                <%  
                                        }else
                                        {
                                %>
                                            <option value="<%=distritoTemporal.getId_distri()%>" ><%=distritoTemporal.getNombre()%></option>
                                <%        
                                        }
  
                                    }
                                %>
                                </select>

                                <label>Direccion:</label>
                                <input type="text" name="txtDireccion" placeholder="Ingrese Direccion" value="<%=estudianteCargado.getDireccion()%>" required class="form-control" />
                            </div>   
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <label>Estado:</label>
                                    <input type="text" name="txtEstado" placeholder="Ingrese Estado" value="<%=estudianteCargado.getEstado()%>" required class="form-control" />
                                    <br>
                                    <a href="#" class="thumbnail">
                                        <img src="img/estu.jpg" width="200"/>
                                    </a>
                                </div>
                            </div>       
                        </div> 
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning" />
                            <input type="reset" value="Deshacer"  class="btn btn-success"/>
                            <input type="button" value="Regresar" onclick="window.location.href='controladorEstudiante'" class="btn btn-info"/>
                        </div> 
                    </div>
                </div>
            </form>
        </form>
    </div>
    </body>
</html>
