<%-- 
    Document   : registrarEstudiante
    Created on : 18-dic-2018, 17:35:22
    Author     : LUIS
--%>

<%@page import="modelo.claseDistrito"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Registro de Estudiantes</title>
    </head>
    <%
        List<claseDistrito> distritos = (List<claseDistrito>)request.getAttribute("distritos");
    %>
    <body>
        <div class="container">
            <form action="controladorEstudiante">
                <div class="row">
                    <div class="col-lg-4">
                        <label>Dni:</label>
                        <input type="number" name="txtDni" placeholder="Ingrese Dni" required class="form-control" />
                        <label>Nombre:</label>
                        <input type="text" name="txtNombre" placeholder="Ingrese Nombre" required class="form-control" />
                        <label>Apellidos:</label>
                        <input type="text" name="txtApellidos" placeholder="Ingrese Apellidos"  required class="form-control" />
                        <label>Genero:</label>
                        <input type="text" name="txtGenero" placeholder="Ingrese Genero"  required class="form-control" />
                        <label>Celular:</label>
                        <input type="number" name="txtCelular" placeholder="Ingrese Celular" required class="form-control" />
                    </div>   
                    <div class="col-lg-4">
                        <label>Correo:</label>
                        <input type="email" name="txtCorreo" placeholder="Ingrese Correo" required class="form-control" />
                        <label>Fecha De Nacimiento:</label>
                        <input type="date" name="txtFecha_nac" placeholder="Ingrese F.nacimiento" required class="form-control" />
                        <label>Lugar De Nacimiento:</label>
                        <input type="text" name="txtLugar_nac" placeholder="Ingrese L.nacimiento" required class="form-control" />
                        <label>Id Distrito:</label>
                        <select name="id_distrito" class="form-control">
                            <%
                                for (claseDistrito distritoTemporal : distritos) {
                            %>                           
                            <option value="<%=distritoTemporal.getId_distri()%>" ><%=distritoTemporal.getNombre()%></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Direccion:</label>
                        <input type="text" name="txtDireccion" placeholder="Ingrese Direccion" required class="form-control" />
                    </div>   
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <label>Estado:</label>
                            <input type="text" name="txtEstado" placeholder="Ingrese Estado" required class="form-control" />
                            <br>
                            <a href="#" class="thumbnail">
                                <img src="img/estu.jpg" width="200"/>
                            </a>
                        </div>
                    </div>       
                </div> 
                <br>
                <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
                </div>     
            </form>
        </div>
    </body>
</html>
