
<%@page import="java.util.List"%>
<%@page import="modelo.claseprovincia"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <%
        List<claseprovincia> provincias = (List<claseprovincia>)request.getAttribute("provincias");
    %>
    <body>
        <div class="container">
            <form action="controladorDistrito">

                <div class="row">
                    <div class="col-lg-4">
                        <label>Provincia:</label>
                        <select name="id_provin" class="form-control">
                                <%
                                    for (claseprovincia provinciaTemporal : provincias) 
                                    {                                              
                                %>
                                            <option value="<%=provinciaTemporal.getId_provin()%>" ><%=provinciaTemporal.getNombre()%></option>
                                <%        
                                    }
                                %>
                        </select>
                        
                        <label>Nombre:</label>    
                        <input type="text" name="txtnombre" placeholder="Ingrese Nombre" required class="form-control" />
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <img src="img/dpd.png" width="200"/>
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="text-justify">
                    <input type="submit" name="accion" value="Registrar" class="btn btn-warning" />
                    <input type="reset" value="Nuevo" class="btn btn-success" />
                </div> 

            </form>
        </div>
    </body>
</html>
