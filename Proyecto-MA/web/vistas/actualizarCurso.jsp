<%@page import="modelo.claseCurso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
       <title>Actualización de Curso</title>
    </head>
    <%
        claseCurso cursoCargado = (claseCurso)request.getAttribute("cursoCargado");
    %>
    <body>
        <div class="container">
            <%@include file="../menu.html" %>
            <form action="controladorCurso">
                <input type="hidden" name="id_curso" value="<%=cursoCargado.getId_curso()%>" />
                <br>
                <div class="panel panel-primary">
                    <div class="panel-heading"><Strong>Formulario de Actualización - Cursos</Strong></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">         
                                <label>Nombre:</label>
                                <input type="text" name="txtNombre" placeholder="Ingrese Nombre" class="form-control" required value="<%=cursoCargado.getNombre()%>" />
                                <label>Horas:</label>
                                <input type="number" name="txtHora" placeholder="Ingrese Hora" class="form-control" required  value="<%=cursoCargado.getNro_hora()%>"/>
                                <label>Creditos:</label>
                                <input type="number" name="txtCreditos" placeholder="Ingrese Creditos" class="form-control" required value="<%=cursoCargado.getNro_creditos()%>" />
                            </div>  
                            <div class="row">
                                <div class="col-xs-6 col-md-4">
                                    <a href="#" class="thumbnail">
                                        <img src="img/car.jpg" width="250"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-justify">
                            <input type="submit" name="accion" value="Actualizar" class="btn btn-warning"/>
                            <input type="reset" value="Deshacer" class="btn btn-success"/>
                            <input type="button" value="Regresar" class="btn btn-info" onclick="window.location.href='controladorCurso'" />
                        </div>        
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>